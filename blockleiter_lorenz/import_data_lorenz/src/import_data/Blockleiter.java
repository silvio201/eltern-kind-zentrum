/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package import_data;

import java.io.Serializable;

/**
 *
 * @author lorry
 */
public class Blockleiter implements Serializable{
    int id;
    Anrede anrede;
    String titel;
    String vorname;
    String nachname;
    String email;
    String festnetz_nummer;
    String firmenName;
    String handyn;
    Adresse adresse;

    public Blockleiter() {
    }
    
    

    public Blockleiter(int id) {
        this.id = id;
    }
    
    

    @Override
    public String toString() {
        return "Blockleiter{" + "id=" + id + ", anrede=" + anrede + ", titel=" + titel + ", vorname=" + vorname + ", nachname=" + nachname + ", email=" + email + ", telefon=" + festnetz_nummer + ", firmenName=" + firmenName +'}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Anrede getAnrede() {
        return anrede;
    }

    public void setAnrede(Anrede anrede) {
        this.anrede = anrede;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return festnetz_nummer;
    }

    public void setTelefon(String telefon) {
        this.festnetz_nummer = telefon;
    }

    public String getFirmenName() {
        return firmenName;
    }

    public void setFirmenName(String firmenName) {
        this.firmenName = firmenName;
    }

    public Adresse getAdresseFK() {
        return adresse;
    }

    public void setAdresseFK(Adresse adresseFK) {
        this.adresse = adresseFK;
    }
    
    
}
