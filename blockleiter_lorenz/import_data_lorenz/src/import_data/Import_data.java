/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package import_data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lorry
 */
public class Import_data {
//Anr;Titel;FamNam;VorNam;Straße;Nr;PLZ;Ort;Festnetz Vowahl;Festnetznummer;HanVw;HanNr;Email
    /**
     * @param args the command line arguments
     */
    static List<Blockleiter> blockleiter;
    static Connection c;

    public static void main(String[] args) {
        try {
            blockleiter = new ArrayList<Blockleiter>();
            System.out.println(System.getProperty("java.home"));
          
            c = DriverManager.getConnection("jdbc:mysql://10.0.0.100:3306/ekiz?zeroDateTimeBehavior=convertToNull&serverTimezone=MET", "dbAdminUser", "FelixLorenzMichi@EKiZ");

            generateBlockleiter();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Import_data.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Import_data.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Import_data.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void generateBlockleiter() throws FileNotFoundException, IOException, SQLException {
        File f = new File("tab_Kursleiter_NEU.csv");
        BufferedReader br = new BufferedReader(new FileReader(f));
        c.setAutoCommit(true);
        String line = br.readLine();
        int id = 1;
        line = br.readLine();

        while (line != null) {

            line = line.replace("\"", "");
            StringBuilder s_temp = new StringBuilder(line);
            line = s_temp.append(";abc;").toString();
            
            String [] fields =line.split(";");
            
           //Anr0;Titel1;FamNam2;VorNam3;Straße4;Nr5;PLZ6;Ort7;Festnetz Vowahl8;Festnetznummer9;HanVw10;HanNr11;Email12
            
            Blockleiter b = new Blockleiter(id); //id
            if (fields[0].contains("rr")) { //ANREDE
                b.anrede = Anrede.HERR;
            } else {
                b.anrede = Anrede.FRAU;
            }
            b.titel = fields[1].replace("0", ""); //TITEL
            b.nachname = fields[2]; //NACHNAME
            b.vorname = fields[3];//VORNAME
            b.adresse = new Adresse(); //ADRESSE-OBJEKT
            b.adresse.straße = fields[4]; //STRASSE
            b.adresse.person_id = id; //person-id
            b.adresse.hausNr = fields[5]; // HAUSNR
            try {
                b.adresse.plz = Integer.parseInt(fields[6]); //Int parse PLZ
            } catch (NumberFormatException e) {
                b.adresse.plz = null;
            }
            b.adresse.wohnort = fields[7]; //WOHNORT
            b.festnetz_nummer = fields[8]+fields[9] ;//TELEFON
            b.festnetz_nummer = b.festnetz_nummer.trim();
            b.handyn = fields[10]+fields[11];
            b.email = fields[12]; //EMAIL
            
            blockleiter.add(b);

            System.out.println(id);
            
            PreparedStatement st_adresse = c.prepareStatement("INSERT INTO adresse(STRASSE,WOHNORT,PLZ,HAUSNR,STIEGE,WOHNUNGSNR,LAND) VALUES(?,?,?,?,?,?,?)");
            
            st_adresse.setString(1, b.adresse.straße);
            st_adresse.setString(2, b.adresse.wohnort);
            if (b.adresse.plz == null) {
                st_adresse.setNull(3, java.sql.Types.INTEGER);
            } else {
                st_adresse.setInt(3, (b.adresse.plz));
            }
            st_adresse.setString(4, b.adresse.hausNr);

            if (b.adresse.stiege == null) {
                st_adresse.setNull(5, java.sql.Types.INTEGER);
            } else {
                st_adresse.setInt(5, b.adresse.stiege);
            }

            if (b.adresse.wohnungsNr == null) {
                st_adresse.setNull(6, java.sql.Types.INTEGER);
            } else {
                st_adresse.setInt(6, b.adresse.wohnungsNr);
            }
            st_adresse.setString(7, b.adresse.land);

            
             

            PreparedStatement st = c.prepareStatement("INSERT INTO blockleiter(Anrede,Titel,Vorname,Nachname,EMail,Handynummer,fkAdresse,Festnetznummer) "
                    + "VALUES(?,?,?,?,?,?,last_insert_id(),?)");
            
            st.setString(1, b.anrede.toString());
            st.setString(2, b.titel);
            st.setString(3, b.vorname);
            st.setString(4, b.nachname);
            st.setString(5, b.email);
            st.setString(6, b.handyn);
            st.setString(7, b.festnetz_nummer);
            id++;
           

        System.out.println(st_adresse.execute());           
        System.out.println(st.execute());
            
            line = br.readLine();
        
        
        System.out.println("finished");
    }
        br.close();
        c.close();
}
}
