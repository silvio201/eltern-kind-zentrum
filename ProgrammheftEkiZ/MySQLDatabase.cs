﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammheftEkiZ
{
    class EkizDatabase
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string user;
        private string password;
        private string port;
        private string connectionString;
        private string sslM;
        public EkizDatabase()
        {
            server = "10.0.0.100";
            user = "dbAdminUser";
            password = "FelixLorenzMichi@EKiZ";
            
            //server = "localhost";
            //user = "root";
            //password = "root";

            database = "ekiz";
            port = "3306";
            sslM = "none";

            connectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", server, port, user, password, database, sslM);

            connection = new MySqlConnection(connectionString);
        }

        public void readDatabase()
        {

            try
            {
                //Regulärer Versand
                Console.WriteLine("Connecting to MySQL...");
                connection.Open();
                DateTime d = DateTime.Now;

                //Console.WriteLine(d.ToString("yyyy-MM-dd HH:mm:ss"));

                string sql = "SELECT email FROM ekiz.erwachsener where Programmheft = b'1'";

                //Console.WriteLine(sql);
                Console.WriteLine("Lese Datenbank");

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                MySqlDataReader rdr = cmd.ExecuteReader();

                List<Email> CurrentListsendEmail = new List<Email>();
                List<Email> CurrentListnoEmail = new List<Email>();

                rdr.Read();

                do
                {
                    String mail = "";
                    if (!rdr.IsDBNull(0))
                    {
                        mail = rdr.GetString("email");
                        if (mail.Contains("@") && mail.Split('@')[1].Length > 0)
                        {
                            CurrentListsendEmail.Add(new Email(mail, "EKIZ - Programmheft für das nächste Semester"));
                        }
                        else
                        {
                            CurrentListnoEmail.Add(new Email(mail, "EKiZ Programmheft für das nächste Semester"));
                        }
                    }
                    else
                    {
                        CurrentListnoEmail.Add(new Email(mail, "EKiZ Programmheft für das nächste Semester"));
                    }
                } while ((rdr.Read()));

                rdr.Close();

                SendMail sendMail = new SendMail();
                sendMail.sendMails(CurrentListsendEmail, CurrentListnoEmail, this);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            Console.WriteLine("Done.");

        }
    }
}
