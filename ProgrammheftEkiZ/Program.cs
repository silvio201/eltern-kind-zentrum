﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammheftEkiZ
{
    class Program
    {
        static void Main(string[] args)
        {
            Process[] processes = Process.GetProcessesByName("OUTLOOK");
            Console.WriteLine("Bitte bestätigen Sie das Sie das Programmheftaussenden wollen!");
            Console.WriteLine("Geben Sie bitte dazu einfach:");
            Console.WriteLine("senden");
            Console.WriteLine("oder");
            Console.WriteLine("nicht senden");
            Console.WriteLine(Encoding.GetEncoding("windows-1250").EncodingName);
            Console.WriteLine("--------------------------------------------------------------");
            string check=Console.ReadLine();

            if (check == "senden")
            {
                if (processes.Length > 0)
                {
                    //EkizDatabase readDatabase --> SendMail = Funktion des Programmes
                    EkizDatabase db = new EkizDatabase();
                    db.readDatabase();
                }
                else
                {
                    Console.WriteLine("Bitte starten Sie Outlook.");
                }
            }
            
        }
    }
}
