﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammheftEkiZ
{
    class SendMail
    {
        string workingBody;
        public SendMail()
        {
        }

        public void sendMails(List<Email> sendMails, List<Email> noMails, EkizDatabase database)
        {

            String path = Directory.GetCurrentDirectory();
            Console.WriteLine(path);
            path = path.Substring(0, path.LastIndexOf("ProgrammheftEkiZ"));
            Console.WriteLine(path);
            path = path + "ProgrammheftEkiZ\\EmailText.htm";

            for (int i = 0; i < sendMails.Count; i++)
            {
                using (StreamReader HtmlReader = new StreamReader(path, Encoding.GetEncoding("windows-1250")))
                {
                    workingBody = HtmlReader.ReadToEnd();
                }
                Email current = sendMails[i];
                Application application = new Application(); //Outlook
                MailItem mailItem = null;

                try
                {
                    mailItem = (MailItem)application.CreateItem(OlItemType.olMailItem);

                    mailItem.Subject = current.subject;
                    mailItem.To = current.mail;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = workingBody;
                    mailItem.Display(false);
                    mailItem.Send();

                    Console.WriteLine("email" + i);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Exception caught in SendMail", ex.ToString());
                    Console.WriteLine("Es ist leider ein Fehler aufgetreten. Stellen Sie sicher das Outlook geöffnet ist!");
                    Environment.Exit(1);
                }

            }
        }
    }
}
