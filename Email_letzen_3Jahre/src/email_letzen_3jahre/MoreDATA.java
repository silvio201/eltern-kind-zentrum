/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email_letzen_3jahre;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdopp
 */
public class MoreDATA {

    public static ArrayList<Erwachsener> listCorrect = new ArrayList<Erwachsener>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        // TODO code application logic here
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("tab_KursAnmeldung.txt"))));
        String line = "TEST";
        String[] semester = new String[]{"K", "J", "I", "H", "G", "F"};
        int counterBesuche = 0;
        ArrayList<Integer> arrayInt = new ArrayList<Integer>();
        try {
            line = br.readLine();

            while (line != null) {

                String[] arrLine = line.split(";");
                
                for (int i = 0; i < semester.length; i++) {
                    if (arrLine[2].startsWith("\"" + semester[i])) {
                        // System.out.println(arrLine[2].toCharArray()[1] + "-" + arrLine[1]);
                        arrayInt.add(Integer.parseInt(arrLine[1]));
                        counterBesuche++;
                    }
                }
                line = br.readLine();
            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(MoreDATA.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.println(line);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        System.out.println("Besuche: " + counterBesuche);
        int counter = 0;
        ArrayList<Integer> emails = new ArrayList<Integer>();
        emails.add(arrayInt.get(0));
        boolean contained = true;

        for (int i = 0; i < arrayInt.size(); i++) {
            for (int x = 0; x < emails.size(); x++) {
                if (arrayInt.get(i).intValue() == emails.get(x).intValue()) {
                    contained = true;
                    break;
                }
            }

            if (!contained) {
                emails.add(arrayInt.get(i));
            }
            contained = false;
        }
        System.out.println("Emails: " + emails.size());

        readFile();

        System.out.println(listCorrect.size());
        ArrayList<Erwachsener> emailsToSends = new ArrayList<>();
        int num;

        for (int i = 0; i < emails.size(); i++) {
            for (int x = 0; x < listCorrect.size(); x++) {
                if (listCorrect.get(x).num == emails.get(i).intValue()) {
                    emailsToSends.add(listCorrect.get(x));
                    listCorrect.remove(x);
                    break;
                }
            }

            //Prozent bis Fertigstellung
            if (i % 50 == 0) {
                System.out.println((i * 100) / emails.size() + "%");
            }
        }

        System.out.println(emails.size());
        int countNotNull = 0;
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File("EMAILS_NAME.txt"))));
            for (int in = 0; in < emailsToSends.size(); in++) {
               
                bw.write(emailsToSends.get(in).getEmail());
                bw.newLine();
                System.out.println("Email " + emailsToSends.get(in).getEmail());
            }
            System.out.println("Vorhande Emails: " + emailsToSends.size());
            bw.write("#Erfolgreich Extrahiert: " + emailsToSends.size() + " von " + emails.size() + " gesamt#");
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void readFile() {
        try {
            BufferedReader buffed = new BufferedReader(new InputStreamReader(new FileInputStream("tab_Adressen.txt"), "ISO-8859-1"));
            String read = buffed.readLine();
            int i = 0;

            while (read != null) {
                if (!read.contains(";")) {
                    read = buffed.readLine();
                    continue;
                }

                String[] stringarray = read.split(";");
                if (stringarray.length >= 18) {
                    Erwachsener erwachsener = new Erwachsener();
                    

                    
                    String s = "";
                    
                    
                    s = stringarray[8];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        if(s.matches("1")){
                            read = buffed.readLine();
                            continue;
                        }
                    }

                    try {
                        erwachsener.num = Integer.parseInt(stringarray[0]);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    s = stringarray[5];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setFamilienName(s);
                    } else {
                        erwachsener.setFamilienName(null);
                    }

                    s = stringarray[6];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setVorname(s);
                    } else {
                        erwachsener.setVorname(null);
                    }

                    try {
                        s = stringarray[26];
                        if (s.contains("\"") && s.length() > 2) {
                            s = s.substring(1, s.length() - 1);
                            if (s.contains("@") && s.contains(".")) {
                                if (s.contains("<") || s.contains("[") || s.contains(" ") || s.contains("]") || s.contains(">")) {
                                    erwachsener.setEmail(null);
                                } else {
                                    erwachsener.setEmail(s);
                                }
                            }
                        } else {
                            erwachsener.setEmail(null);
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        erwachsener.setEmail(null);
                    }

                    if (erwachsener.getEmail() != null) {
                        listCorrect.add(erwachsener);
                    }

                } else {
                    int y = listCorrect.size() - 1;
                    listCorrect.remove(y);
                }

                read = buffed.readLine();
            }
            buffed.close();

        } catch (FileNotFoundException ex) {
            //Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
