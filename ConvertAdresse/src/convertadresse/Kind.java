/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertadresse;

import java.rmi.UnexpectedException;
import java.util.ArrayList;

/**
 *
 * @author mdopp
 */
public class Kind {
    private String vn;
    private String nn;
    private String gebDatum;

    public Kind() {
      
    }

    public String getVn() {
        return vn;
    }

    public void setVn(String vn) {
        this.vn = vn;
    }

    public String getNn() {
        return nn;
    }

    public void setNn(String nn) {
        this.nn = nn;
    }

    public String getGebDatum() {
        return gebDatum;
    }

    public void setGebDatumFormatInsert(String date) throws UnexpectedException {
        StringBuilder sb=new StringBuilder();
        //System.out.println(""+date);
        String [] arrDate=date.split("\\.");
        int month=Integer.parseInt(arrDate[0]);
        int year = Integer.parseInt(arrDate[1]);
        if(year > 1900){
            sb.append(year+"-");
             if(month<=12 && month>=1){
                if(month < 10){
                    sb.append("0"+month+"-1");
                }else{
                    sb.append(month+"-1");
                }
            }
        }
        if(sb.toString().split("-").length<3) throw new UnexpectedException("Problem occured with date "+gebDatum);
        gebDatum=sb.toString();
    }
    
    
}
