/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertadresse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.rmi.UnexpectedException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anzen
 */
public class ConvertAdresse {

    public static LinkedList<Erwachsener> listCorrect = new LinkedList<Erwachsener>();
    public static LinkedList<Adresse> listAdresse = new LinkedList<Adresse>();
    public static String falscheDaten = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        readFile();
    }

    public static void readFile() {
        try {
            String[] formerstringarray = null;
            BufferedReader buffed = new BufferedReader(new InputStreamReader(new FileInputStream("tab_Adressen.txt"), "ISO-8859-1"));
            String read = buffed.readLine();
            int i = 0;
            int countRelevanteErwachsene=0;
            int countKinderSuccesfullyGenerated=0;
            int countKinderInsgesamt=0;
          
            while (read != null) {
                if (!read.contains(";")) {
                    read = buffed.readLine();
                    continue;
                }
                
                //28 Spalten --> index 0 - 28
                String[] stringarray = read.split(";");
                if (stringarray.length >= 18) {
                    Erwachsener erwachsener = new Erwachsener();
                    Adresse adresse = new Adresse();

                    String s = "";

                    s = stringarray[8];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        if (s.matches("1")) {
                            read = buffed.readLine();
                            continue;
                        }
                        countRelevanteErwachsene++;
                    }

                    s = stringarray[1];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setAnrede(s);
                    } else {
                        erwachsener.setAnrede(null);
                    }

                    s = stringarray[3];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setTitel(s);
                    } else {
                        erwachsener.setTitel(null);
                    }

                    s = stringarray[2];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setFirmenName(s);
                    } else {
                        erwachsener.setFirmenName(null);
                    }

                    s = stringarray[5];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setFamilienName(s);
                    } else {
                        erwachsener.setFamilienName(null);
                    }

                    s = stringarray[6];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        erwachsener.setVorname(s);
                    } else {
                        erwachsener.setVorname(null);
                    }

                    s = stringarray[13];
                    String s2 = stringarray[14];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        s = s + s2;
                        if (s.equals("")) {
                            erwachsener.setFestnetz(null);
                        } else {
                            erwachsener.setFestnetz(s);
                        }
                    } else {
                        erwachsener.setFestnetz(null);
                    }

                    s = stringarray[15];
                    s2 = stringarray[16];

                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        s = s + s2;
                        if (s.equals("")) {
                            erwachsener.setHandy(null);
                        } else {
                            erwachsener.setHandy(s);
                        }
                    } else {
                        erwachsener.setHandy(null);
                    }

                    s = stringarray[18];
                    if (s.length() == 17 || s.length() == 18 || s.length() == 19) {
                        String[] split = s.split("\\.");
                        s = split[2].substring(0, 4);
                        s = s + "-" + split[1];
                        s = s + "-" + split[0];
                        erwachsener.setMtgSeit(s);
                    }

                    try {
                        s = stringarray[26];
                        if (s.contains("\"") && s.length() > 2) {
                            s = s.substring(1, s.length() - 1);
                            if (s.contains("@") && s.contains(".")) {
                                if (s.contains("<") || s.contains("[") || s.contains(" ") || s.contains("]") || s.contains(">")) {

                                } else {
                                    erwachsener.setEmail(s);
                                }
                            }
                        } else {
                            erwachsener.setEmail(null);
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        erwachsener.setEmail(null);
                    }

                    s = stringarray[9];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        adresse.setStrasse(s);
                    } else {
                        adresse.setStrasse(null);
                    }

                    s = stringarray[10];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        adresse.setHausnummer(s);
                    } else {
                        adresse.setHausnummer(null);
                    }

                    s = stringarray[11];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        adresse.setPlz(s);
                    } else {
                        adresse.setPlz(null);
                    }

                    s = stringarray[12];
                    if (s.contains("\"")) {
                        s = s.substring(1, s.length() - 1);
                        adresse.setOrt(s);
                    } else {
                        adresse.setOrt(null);
                    }
                    i = i + 1;
                    System.out.println(i);
                    if (erwachsener.getFirmenName() != null) {
                        if (erwachsener.getFirmenName().equals(".")) {
                            erwachsener.setFirmenName(null);
                        }
                    }
                    
                    if (erwachsener.getFirmenName() != null && erwachsener.getVorname() == null && erwachsener.getFamilienName() == null) {
                        erwachsener.setFamilienName("kein Ansprechpartner");
                        erwachsener.setVorname("kein Ansprechpartner");
                    }
                    
                    if (erwachsener.getVorname() == null && erwachsener.getFirmenName() == null && erwachsener.getFamilienName() == null) {
                        falscheDaten = falscheDaten + stringarray[0] + "; Kein Firmenname, Familienname oder Vorname" + "\n";
                        read = buffed.readLine();
                        continue;
                    }

                    if (erwachsener.getFestnetz() == null && erwachsener.getHandy() == null && erwachsener.getEmail() == null) {
                        falscheDaten = falscheDaten + stringarray[0] + ";"+ stringarray[5] + "; Kein Festnetz, Telefonnummer oder Email" + "\n";
                        read = buffed.readLine();
                        continue;
                    }
                    if (erwachsener.getFamilienName().equals("Doppelbauer")) {
                        System.out.println("");
                    }
                    if (adresse.getHausnummer() == null || adresse.getHausnummer().length() > 10 || adresse.getOrt() == null || adresse.getPlz() == null || adresse.getStrasse() == null) {
                        falscheDaten = falscheDaten + stringarray[0] + ";"+stringarray[5] +"; Ungültige Adresse" + "\n";
                        read = buffed.readLine();
                        continue;
                    }

                    if (!(erwachsener.getAnrede().equals("Herr") || erwachsener.getAnrede().equals("Frau") || erwachsener.getAnrede().equals("An"))) {
                        falscheDaten = falscheDaten + stringarray[0] + ";"+stringarray[5] + "; Ungültige Anrede" + "\n";
                        read = buffed.readLine();
                        continue;
                    }
                    
                    //Kinder
                    s=stringarray[stringarray.length - 1];
                    if(stringarray.length >= 29 && !s.contains("!") && !s.contains("@") && !s.trim().equals("") && !s.equals(adresse) && !s.contains("/") && s.contains("20")){
                        countKinderInsgesamt++;
                    if(s.contains("-") && s.contains(".") && !s.contains(":")){
                        try{
                        s=s.replace(" ","");
                        s=s.replace("\"", "");
                        String[] arr=s.split(",");
                        for (String arr1 : arr) {
                            String[] sep=arr1.split("-");
                            if(sep.length != 2) {
                                falscheDaten = falscheDaten + stringarray[0]+";"+stringarray[5] + "; Kinder Datums-Syntax ist nicht richtig vn-M.YYYY Bsp. (JonathanAaron-6.2005,PhilippFrederik-9.2008,Helenna-12.2011)" +";"+s+"\n";
                                read = buffed.readLine();
                                continue;
                            }
                            char[] vnArr = sep[0].toCharArray();
                            Kind k = new Kind();
                            
                            StringBuilder vnStrBuilder = new StringBuilder();
                            vnStrBuilder.append(vnArr[0]);
                            for(int x = 1; x<vnArr.length; x++){
                                if(Character.isUpperCase(vnArr[x])) vnStrBuilder.append(" ");
                                vnStrBuilder.append(vnArr[x]);
                            }
                            k.setVn(vnStrBuilder.toString());
                            k.setNn(erwachsener.getFamilienName());
                            k.setGebDatumFormatInsert(sep[1]);
                            
                            
                            erwachsener.addKind(k);
                        }
                        countKinderSuccesfullyGenerated++;
                        }catch(Exception e){
                                 //e.printStackTrace();
                                 falscheDaten = falscheDaten + stringarray[0]+";"+stringarray[5] + "; Kinder Syntax ist nicht richtig vn-M.YYYY Bsp. (JonathanAaron-6.2005,PhilippFrederik-9.2008,Helenna-12.2011)" +";"+s+ "\n";
                                 read=buffed.readLine();
                                 
                                 continue;
                            }
                    }else{
                        falscheDaten = falscheDaten + stringarray[0]+";"+stringarray[5] + "; Kinder Syntax ist nicht richtig vn-M.YYYY Bsp. (JonathanAaron-6.2005,PhilippFrederik-9.2008,Helenna-12.2011)" +";"+s+ "\n";
                        read = buffed.readLine();
                        continue;
                    }
                    }

                    listCorrect.add(erwachsener);
                    listAdresse.add(adresse);
                } else {
                    int y = listCorrect.size() - 1;
                    listCorrect.remove(y);
                    listAdresse.remove(y);
                    int test = Integer.parseInt(formerstringarray[0]);
                    falscheDaten = falscheDaten + test + "; Datensatz falsch formatiert" + "\n";
                }

                read = buffed.readLine();
                formerstringarray = stringarray;
            }
            buffed.close();
            System.out.println("Kinder richtig erzeugt: "+countKinderSuccesfullyGenerated+" von insgesamt "+ countKinderInsgesamt + " | Sind fehlerhaft: "+(countKinderInsgesamt-countKinderSuccesfullyGenerated));
            System.out.println("Erwachsene erzeugt: "+listCorrect.size()+" von insgesamt "+countRelevanteErwachsene+" | Sind fehlerhaft: "+(countRelevanteErwachsene-listCorrect.size()));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        }

        writeObject();
        addtoDatabase();
    }

    private static void writeObject() {
        try {
            BufferedWriter bw;

            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("falscheDaten.csv"), "ISO-8859-1"));
            bw.write(falscheDaten);
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void addtoDatabase() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://10.0.0.100:3306/ekiz?zeroDateTimeBehavior=convertToNull&serverTimezone=MET", "dbAdminUser", "FelixLorenzMichi@EKiZ");
            //Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ekiz?zeroDateTimeBehavior=convertToNull&serverTimezone=MET", "root", "root");

            connection.setAutoCommit(false);
            for (int i = 0; i < listAdresse.size(); i++) {
                PreparedStatement stm = connection.prepareStatement("INSERT INTO adresse (strasse, wohnort, plz, hausnr) VALUES(?,?,?,?)");
                Adresse adr = listAdresse.get(i);
                stm.setString(1, adr.getStrasse());
                stm.setString(2, adr.getOrt());
                stm.setInt(3, Integer.parseInt(adr.getPlz()));
                stm.setString(4, adr.getHausnummer());
                stm.execute();
                Erwachsener e = listCorrect.get(i);
                stm = connection.prepareStatement("INSERT INTO erwachsener (fkadresse, anrede, titel, nachname, vorname, handynummer, festnetz, email, mitgliedschaft_eingezahlt, firmenname) VALUES(LAST_INSERT_ID(), ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                stm.setString(1, e.getAnrede());
                stm.setString(2, e.getTitel());
                stm.setString(3, e.getFamilienName());
                stm.setString(4, e.getVorname());
                stm.setString(5, e.getHandy());
                stm.setString(6, e.getFestnetz());
                stm.setString(7, e.getEmail());
                if (e.getMtgSeit() == null) {
                    stm.setDate(8, Date.valueOf("1991-1-1"));
                } else {
                    stm.setDate(8, Date.valueOf(e.getMtgSeit()));
                }
                stm.setString(9, e.getFirmenName());
                //System.out.println(i);
                if(i == 156){
                    System.out.println("");
                }
                stm.execute();
                ResultSet rs=stm.executeQuery("Select LAST_INSERT_ID() as id from Dual");
                rs.next();
                int erwID=rs.getInt("id");              
                rs.close();
                LinkedList<Kind> kinder=e.getKinder();
                for(Kind kind: kinder){
                    stm = connection.prepareStatement("INSERT INTO KIND (Vorname, Nachname, Geburtsdatum) VALUES (?, ?, ?);");
                    stm.setString(1,kind.getVn());
                    stm.setString(2, kind.getNn());
                    stm.setString(3, kind.getGebDatum());
                    stm.executeUpdate();
                    
                    /*rs=stm.executeQuery("Select LAST_INSERT_ID() as id from Dual");
                    rs.next();
                    System.out.println(rs.getInt("id"));  
                    rs.close();
                    */
                    
                    stm = connection.prepareStatement("INSERT INTO ERWACHSENER_HAS_KIND (Erwachsener_idErwachsener, Kind_idKind) VALUES (?, LAST_INSERT_ID())");
                    stm.setInt(1, erwID);
                    stm.executeUpdate();
                }
               
            }
            connection.commit();
            connection.close();
            System.out.println("Done!");
        } catch (SQLException ex) {
            Logger.getLogger(ConvertAdresse.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
