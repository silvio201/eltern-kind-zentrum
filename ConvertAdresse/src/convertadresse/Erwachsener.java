/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertadresse;

import java.util.LinkedList;

/**
 *
 * @author anzen
 */
public class Erwachsener {
    private String anrede;
    private String titel;
    private String familienName;
    private String vorname;
    private String firmenName;
    private String festnetz;
    private String handy;
    private String mtgSeit;
    private String email;
    private LinkedList<Kind> kinder;
    
    public Erwachsener(String anrede, String titel, String familienName, String vorname, String firmenName, String festnetz, String handy, String email) {
        this();
        this.anrede = anrede;
        this.titel = titel;
        this.familienName = familienName;
        this.vorname = vorname;
        this.firmenName = firmenName;
        this.festnetz = festnetz;
        this.handy = handy;
        this.email = email;
    }

    public Erwachsener() {
        kinder = new LinkedList<>();
    }

    public LinkedList<Kind> getKinder() {
        return kinder;
    }

    public void setKinder(LinkedList<Kind> kinder) {
        this.kinder = kinder;
    }
    
    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getFamilienName() {
        return familienName;
    }

    public void setFamilienName(String familienName) {
        this.familienName = familienName;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getFirmenName() {
        return firmenName;
    }

    public void setFirmenName(String firmenName) {
        this.firmenName = firmenName;
    }

    public String getFestnetz() {
        return festnetz;
    }

    public void setFestnetz(String festnetz) {
        this.festnetz = festnetz;
    }

    public String getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMtgSeit() {
        return mtgSeit;
    }

    public void setMtgSeit(String mtgSeit) {
        this.mtgSeit = mtgSeit;
    }
    
    public void addKind(Kind k){
        kinder.add(k);
    }

    @Override
    public String toString() {
        return "Erwachsener{" + "anrede=" + anrede + ", titel=" + titel + ", familienName=" + familienName + ", vorname=" + vorname + ", firmenName=" + firmenName + ", festnetz=" + festnetz + ", handy=" + handy + ", mtgSeit=" + mtgSeit + ", email=" + email + '}';
    }
    
    
    
    
    
}
