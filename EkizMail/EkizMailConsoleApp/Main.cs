﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace EkizMailConsoleApp
{
    class MainConsole
    {
        static void Main(string[] args)
        {
            Process[] processes = Process.GetProcessesByName("OUTLOOK");
            if (processes.Length > 0)
            {
                //EkizDatabase readDatabase --> SendMail --> updateDatabase = Funktion des Programmes
                EkizDatabase db = new EkizDatabase();
                db.readDatabase();
            }
            else
            {
                MessageBox.Show("Bitte starten Sie Outlook.");
            }
        }
    }
}
