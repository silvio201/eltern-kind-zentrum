﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EkizMailConsoleApp
{
    class EkizDatabase
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string user;
        private string password;
        private string port;
        private string connectionString;
        private string sslM;
        public EkizDatabase()
        {
            server = "10.0.0.100";
            //server = "localhost";
            database = "ekiz";
            user = "dbAdminUser";
            password = "FelixLorenzMichi@EKiZ";
            //user = "root";
            //password = "root";
            port = "3306";
            sslM = "none";

            connectionString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", server, port, user, password, database, sslM);

            connection = new MySqlConnection(connectionString);
        }

        public void readDatabase()
        {

            try
            {
                //Regulärer Versand
                Console.WriteLine("Connecting to MySQL...");
                connection.Open();
                DateTime d = DateTime.Now;
                
                //Console.WriteLine(d.ToString("yyyy-MM-dd HH:mm:ss"));

                string sql = "SELECT *  FROM block " +
                "Join(Select distinct fkBlock From Termin where TerminStart BETWEEN @val1 AND @val2) as sub on sub.fkblock = block.idBlock " +
                "Join Kurs on block.fkKurs = idKurs " +
                //"Join(Select * from teilnehmer where teilnehmer.EmailVersendet = b'0') AS subTeil on subTeil.Block_idBlock = block.idBlock " +
                "Join(Select * from teilnehmer) AS subTeil on subTeil.Block_idBlock = block.idBlock " +
                "Join Erwachsener on subTeil.Erwachsener_idErwachsener = idErwachsener " +
                "Order by idBlock asc, idTeilnehmer asc;";

                //Console.WriteLine(sql);
                Console.WriteLine("Lese Datenbank");

                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@val1", d.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("@val2", d.AddDays(10).ToString("yyyy-MM-dd HH:mm:ss"));

                MySqlDataReader rdr = cmd.ExecuteReader();

                List<Email> ListsendEmail = new List<Email>();
                List<Email> ListnoEmail = new List<Email>();

                List<Email> CurrentListsendEmail = new List<Email>();
                List<Email> CurrentListnoEmail = new List<Email>();

                rdr.Read();

                int idBlock = rdr.GetInt32("idBlock");
                int minAnzahl = rdr.GetInt32("MinAnzahlTeilnehmer");
                int counterAnzahlProBlock = 0;
                //Sortiert nach Block ändert sich also idBlock dann beginnt ein neuer Block = neuer counter welcher bestimmt wie viele Emails gesendet werden denn wenn der 
                //Counter MaxAnzahl für diesen Block überschreibtet dann werden diese Email nicht gesendet weil sich ja diese Personen noch auf der Warteliste befinden.
                do
                {
                    if (idBlock != rdr.GetInt32("idBlock"))
                    {

                        if (counterAnzahlProBlock >= minAnzahl)
                        {
                            ListsendEmail.AddRange(CurrentListsendEmail);
                            ListnoEmail.AddRange(CurrentListnoEmail);
                        }

                        CurrentListnoEmail = new List<Email>();
                        CurrentListsendEmail = new List<Email>();

                        counterAnzahlProBlock = 0;
                        idBlock = rdr.GetInt32("idBlock");
                        //Console.WriteLine("" + minAnzahl);
                        minAnzahl = rdr.GetInt32("MinAnzahlTeilnehmer");
                        //Console.WriteLine("" + minAnzahl);
                    }

                    counterAnzahlProBlock++;

                    if (counterAnzahlProBlock <= rdr.GetInt32("MaxAnzahlTeilnehmer"))
                    {
                        double preis = 0;


                        if (!rdr.IsDBNull(26))
                        {
                            DateTime dateMitgliedschaft = rdr.GetDateTime("Mitgliedschaft_eingezahlt").AddYears(1);
                            int i = dateMitgliedschaft.CompareTo(DateTime.Now);

                            if (i >= 0)
                            {

                                preis = rdr.GetDouble("Mitgliedspreis");
                            }
                            else {
                                preis = rdr.GetDouble("Preis");
                            }
                        }

                        

                        String mail;
                        if (!rdr.IsDBNull(25))
                        {
                            mail = rdr.GetString("email");
                            if (mail.Contains("@")){

                                if (!rdr.GetBoolean("EmailVersendet"))
                                {
                                    CurrentListsendEmail.Add(new Email(rdr.GetString("Nachname"), rdr.GetString("Kursname"), preis, mail, "Kursbestätigung für " + rdr.GetString("Kursname"), rdr.GetInt16("fkBlock"), rdr.GetInt16("idTeilnehmer")));
                                }
                            }
                            else
                            {
                                CurrentListnoEmail.Add(new Email(rdr.GetString("Nachname"), rdr.GetString("Kursname"), preis, mail, "Kursbestätigung für " + rdr.GetString("Kursname"), rdr.GetInt16("fkBlock"), rdr.GetInt16("idTeilnehmer")));
                            }
                        }
                        else
                        {
                            CurrentListnoEmail.Add(new Email(rdr.GetString("Nachname"), rdr.GetString("Kursname"), preis, "KEIN EMAIL", "Kursbestätigung für " + rdr.GetString("Kursname"), rdr.GetInt16("fkBlock"), rdr.GetInt16("idTeilnehmer")));
                        }
                    }
                    else
                    {
                        continue;
                    }
                } while ((rdr.Read()));

                rdr.Close();

                if (counterAnzahlProBlock >= minAnzahl)
                {
                    ListsendEmail.AddRange(CurrentListsendEmail);
                    ListnoEmail.AddRange(CurrentListnoEmail);
                }


                //Nicht gesendete Emails

                String sqlStartTermin = "Select Min(TerminStart) as TerminStart from Termin where fkBlock=@val1";
                MySqlCommand cmdStartTermin = new MySqlCommand(sqlStartTermin, connection);
                for (int i = 0; i < ListsendEmail.Count; i++)
                {
                    Email current = ((Email)ListsendEmail[i]);
                    cmdStartTermin.Parameters.AddWithValue("@val1", current.fkBlock);
                    MySqlDataReader reader = cmdStartTermin.ExecuteReader();
                    reader.Read();
                    current.datum = ((DateTime)reader.GetDateTime("TerminStart").Date).ToString("dd.MM.yyyy");
                    current.uhrzeit = reader.GetDateTime("TerminStart").TimeOfDay.ToString();
                    current.uhrzeit = current.uhrzeit.Substring(0, current.uhrzeit.Length - 3);
                    reader.Close();
                    cmdStartTermin.Parameters.Clear();
                }
               

                cmdStartTermin = new MySqlCommand(sqlStartTermin, connection);
                for (int i = 0; i < ListnoEmail.Count; i++)
                {
                    Email current = ((Email)ListnoEmail[i]);
                    cmdStartTermin.Parameters.AddWithValue("@val1", current.fkBlock);
                    MySqlDataReader reader = cmdStartTermin.ExecuteReader();
                    reader.Read();
                    current.datum = ((DateTime)reader.GetDateTime("TerminStart").Date).ToString("dd.MM.yyyy");
                    current.uhrzeit = reader.GetDateTime("TerminStart").TimeOfDay.ToString();
                    current.uhrzeit = current.uhrzeit.Substring(0, current.uhrzeit.Length - 3);
                    reader.Close();
                    cmdStartTermin.Parameters.Clear();
                }
                connection.Close();

                SendMail sendMail = new SendMail();
                sendMail.sendMails(ListsendEmail, ListnoEmail, this);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            Console.WriteLine("Done.");

        }

        public void updateDatabase(int idTeilnehmer)
        {
            String sqlUpdate = "UPDATE `ekiz`.`teilnehmer` SET `EmailVersendet` = b'1' " +
                "WHERE (`idTeilnehmer` = @valID)";
            connection.Open();
            MySqlCommand updateTeilnehmer = new MySqlCommand(sqlUpdate, connection);
            updateTeilnehmer.Parameters.AddWithValue("@valID", idTeilnehmer);
            int rowsAffected = updateTeilnehmer.ExecuteNonQuery();
            if (rowsAffected > 1)
            {
                throw new Exception("Mehr als einen Datensatz geändert");
            }
            connection.Close();
        }

    }
}
