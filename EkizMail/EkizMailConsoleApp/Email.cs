﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EkizMailConsoleApp
{
    class Email
    {
        public String nachname;
        public String kursName;

        public double Preis;
        public String datum;
        public String uhrzeit;
        public String mail;
        public String subject;
        public int fkBlock;
        public int idTeilnehmer;

        public Email(String nachname, String kursName, double Preis, String mail, String subject, int fkBlock, int idTeilnehmer) {
            this.nachname = nachname;
            this.kursName = kursName;
            this.Preis = Preis;
            this.datum = datum;
            this.uhrzeit = uhrzeit;
            this.mail = mail;
            this.subject = subject;
            this.fkBlock = fkBlock;
            this.idTeilnehmer = idTeilnehmer;
        }


        public String getPreisToString() {
            if (Preis > 0) {
                return Preis.ToString("N2") + " €";
            }
            return "freiwilliger Kostenbeitrag";
        }

    }
}
