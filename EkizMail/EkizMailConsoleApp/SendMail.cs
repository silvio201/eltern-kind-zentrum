﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Outlook;
using System.IO;
using Exception = System.Exception;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Outlook.Application;

namespace EkizMailConsoleApp
{
    class SendMail
    {
        string workingBody;
        public SendMail()
        {
        }

        private bool boolSendEvent;

        public void sendMails(List<Email> sendMails, List<Email> noMails, EkizDatabase database)
        {

            String path = Directory.GetCurrentDirectory();
            Console.WriteLine(path);
            path = path.Substring(0, path.LastIndexOf("ekiz"));
            Console.WriteLine(path);
            path = path + "EkizMail\\EmailText.html";
          
            for (int i = 0; i < sendMails.Count; i++)
            {
                using (StreamReader HtmlReader = new StreamReader(path))
                {
                    workingBody = HtmlReader.ReadToEnd();
                }
                Email current = sendMails[i];
                FormatHtmlText(current.nachname, current.kursName, current.getPreisToString(), current.datum, current.uhrzeit);
                Console.WriteLine(current.nachname);
                Application application = new Application(); //Outlook
                MailItem mailItem = null;

                try
                {
                    mailItem = (MailItem)application.CreateItem(OlItemType.olMailItem);

                    mailItem.Subject = current.subject;
                    mailItem.To = current.mail;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = workingBody;


                    //Outlook send event
                    ((ItemEvents_10_Event)mailItem).Send += new ItemEvents_10_SendEventHandler(ThisAddIn_Send);
                    boolSendEvent = false;
                    mailItem.Display(true);

                    //Outlook send event
                    ((ItemEvents_10_Event)mailItem).Send += new ItemEvents_10_SendEventHandler(ThisAddIn_Send);

                    if (boolSendEvent)
                    {
                        database.updateDatabase(current.idTeilnehmer);
                        Console.WriteLine("Sent Happened");
                    }
                    else
                    {
                        if (mailItem.Saved)
                        {
                            database.updateDatabase(current.idTeilnehmer);
                            Console.WriteLine("Save Happens");
                        }
                        else
                        {
                            Console.WriteLine("Nothing Happens");
                        }
                    }

                    Console.WriteLine("email" + i);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Exception caught in SendMail", ex.ToString());
                    MessageBox.Show("Es ist leider ein Fehler aufgetreten. Stellen Sie sicher das Outlook geöffnet ist!");
                    Environment.Exit(1);
                }
                
            }

            if(noMails.Count >= 1) MessageBox.Show("Bei Folgenden Emails gibt es keine Email Adresse!");

            for (int i = 0; i < noMails.Count; i++)
            {
               
                using (StreamReader HtmlReader = new StreamReader(path))
                {
                    workingBody = HtmlReader.ReadToEnd();
                }
                Email current = noMails[i];
                FormatHtmlText(current.nachname, current.kursName, current.Preis.ToString("N2") + " €", current.datum, current.uhrzeit);
                //Console.WriteLine(current.nachname);
                Application application = new Application(); //Outlook
                MailItem mailItem = null;

                try
                {

                    mailItem = (MailItem)application.CreateItem(OlItemType.olMailItem);

                    mailItem.Subject = current.subject;
                    mailItem.To = current.mail;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = workingBody;


                    //Outlook send event
                    ((ItemEvents_10_Event)mailItem).Send += new ItemEvents_10_SendEventHandler(ThisAddIn_Send);
                    boolSendEvent = false;
                    mailItem.Display(true);

                    //Outlook send event
                    ((ItemEvents_10_Event)mailItem).Send += new ItemEvents_10_SendEventHandler(ThisAddIn_Send);

                    if (boolSendEvent)
                    {
                        database.updateDatabase(current.idTeilnehmer);
                        Console.WriteLine("Sent Happened");
                    }
                    else
                    {
                        if (mailItem.Saved)
                        {
                            database.updateDatabase(current.idTeilnehmer);
                            Console.WriteLine("Save Happens");
                        }
                        else
                        {
                            Console.WriteLine("Nothing Happens");
                        }
                    }

                    Console.WriteLine("email" + i);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Exception caught in SendMail", ex.ToString());
                    MessageBox.Show("Es ist leider ein Fehler aufgetreten. Stellen Sie sicher das Outlook geöffnet ist!");
                    Environment.Exit(1);
                }


            }
        }

        private void ThisAddIn_Send(ref bool Cancel)
        {
            Console.WriteLine("sent");
            boolSendEvent = true;
        }

        public string FormatHtmlText(string Nachname, string Kurstitel, string Preis, string Datum, string Uhrzeit)
        {
            //Hello <b>{UserName}</b>,<br /><br />
            workingBody = workingBody.Replace("{Nachname}", Nachname);
            workingBody = workingBody.Replace("{Kurstitel}", Kurstitel);
            workingBody = workingBody.Replace("{Preis}", Preis);
            workingBody = workingBody.Replace("{Datum}", Datum);
            workingBody = workingBody.Replace("{Uhrzeit}", Uhrzeit);
            return workingBody;
        }


    }
}


