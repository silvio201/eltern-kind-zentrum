/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import GUI_MainFrame.MainFrame;
import Objects.Anrede;
import Objects.Block;
import Objects.Blockleiter;
import Objects.Kurs;
import Objects.Semester;
import UserVerwaltung.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerListModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author lorenz
 */
public class AddBlockZuKurs extends javax.swing.JDialog {

    /**
     * Creates new form AddBlockZuKurs
     */
    private int idKurs;

    public AddBlockZuKurs(java.awt.Frame parent, boolean modal) {
        super(parent, "neuer Block", modal);
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
        this.idKurs = idKurs;
        setData(null);

    }

    public AddBlockZuKurs(java.awt.Frame parent, int idKurs, boolean modal) {
        super(parent, "neuer Block", modal);
        initComponents();
        this.idKurs = idKurs;
        setData(null);

    }

    public AddBlockZuKurs(java.awt.Frame parent, int idKurs, Block b, boolean modal) {
        super(parent, "neuer Block", modal);
        initComponents();
        this.idKurs = idKurs;
        setData(b);
    }

    private void setData(Block prev_block) {
        DefaultListModel dlm_blockleiter = null;
        try {
            ResultSet rs_blockleiter_list = User.getConnectionFromUser().createStatement().executeQuery("Select * from Blockleiter Order by nachname ");
            dlm_blockleiter = new DefaultListModel<Blockleiter>();
            while (rs_blockleiter_list.next()) {
                Blockleiter bleiter = new Blockleiter(Anrede.valueOf(rs_blockleiter_list.getString("Anrede")), rs_blockleiter_list.getString("Vorname"), rs_blockleiter_list.getString("nachname"), -1);
                bleiter.setIdBlockleiter(rs_blockleiter_list.getInt("idBlockleiter"));
                dlm_blockleiter.addElement(bleiter);
            }
            jListBlockleiter.setModel(dlm_blockleiter);
            jSpinnerSemester.setModel(new SpinnerListModel(Semester.values())); //jspinner 
            if (prev_block != null) {
                ResultSet rs_prev_blockleiter = User.getConnectionFromUser().createStatement().executeQuery(
                        "Select * from Blockleiter where idBlockleiter=" + prev_block.getFkBlockleiter() + " Order by idBlockleiter ");
                rs_prev_blockleiter.next();
                Blockleiter prev_blockleiter = new Blockleiter();
                prev_blockleiter.setIdBlockleiter(rs_prev_blockleiter.getInt("idBlockleiter"));
                int curr=0;
                for (int i = 1; i < dlm_blockleiter.getSize(); i++) { //ermitteln des letzten blockleiters
                    curr = ((Blockleiter) dlm_blockleiter.getElementAt(i)).getIdBlockleiter();
                    if(curr == prev_block.getFkBlockleiter()){
                        curr = i;
                        break;
                    }
                }
                jTextFieldMinTeilnehmer.setText("" + prev_block.getMinAnzahlTeilnehmer());
                jTextFieldMaxTeilnehmer.setText("" + prev_block.getMaxAnzahlTeilnehmer());
                jFormattedTextFieldPreis.setValue(prev_block.getPreis());
                jFormattedTextFieldMitgliedspreis.setValue(prev_block.getMitgliedspreis());
                jListBlockleiter.setSelectedIndex(curr);
                jSpinnerSemester.setValue(prev_block.getSemester());
            }
            if (LocalDate.now().isBefore(LocalDate.of(LocalDate.now().getYear(), 4, 20)) && LocalDate.now().isAfter(LocalDate.of(LocalDate.now().getYear() - 1, 9, 1))) {
                jSpinnerSemester.setValue(Semester.FP);
            } else {
                jSpinnerSemester.setValue(Semester.HP);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AddBlockZuKurs.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ((DefaultEditor) jSpinnerSemester.getEditor()).getTextField().setEditable(false);
            User.closeConnection();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldMaxTeilnehmer = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSpinnerSemester = new javax.swing.JSpinner();
        jFormattedTextFieldPreis = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jFormattedTextFieldMitgliedspreis = new javax.swing.JFormattedTextField();
        jTextFieldMinTeilnehmer = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButtonOK = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListBlockleiter = new javax.swing.JList<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaAnmkerung = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Teilnehmer");

        jTextFieldMaxTeilnehmer.setMinimumSize(new java.awt.Dimension(60, 29));

        jLabel2.setText("Semester");

        jLabel3.setText("Kursleiternummer:");

        jLabel4.setText("Preis");

        jFormattedTextFieldPreis.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.##"))));

        jLabel5.setText("Mitgliedspreis");

        jFormattedTextFieldMitgliedspreis.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.##"))));

        jTextFieldMinTeilnehmer.setMinimumSize(new java.awt.Dimension(60, 29));

        jLabel6.setText("bis");

        jLabel7.setText("von");

        jButtonOK.setBackground(new java.awt.Color(255, 255, 255));
        jButtonOK.setText("OK");
        jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOKActionPerformed(evt);
            }
        });

        jLabel8.setText("Anmerkung");

        jListBlockleiter.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListBlockleiter.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jListBlockleiter);

        jTextAreaAnmkerung.setColumns(20);
        jTextAreaAnmkerung.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jTextAreaAnmkerung.setRows(5);
        jScrollPane1.setViewportView(jTextAreaAnmkerung);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(240, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSpinnerSemester, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                            .addComponent(jFormattedTextFieldPreis, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jFormattedTextFieldMitgliedspreis, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                                    .addComponent(jLabel7))
                                .addComponent(jScrollPane2))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldMinTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(6, 6, 6)
                            .addComponent(jTextFieldMaxTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(14, 14, 14))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(292, 292, 292))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(113, 113, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 333, Short.MAX_VALUE)
                            .addComponent(jButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jSpinnerSemester, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFormattedTextFieldPreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFormattedTextFieldMitgliedspreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextFieldMaxTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldMinTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel2)
                    .addGap(27, 27, 27)
                    .addComponent(jLabel4)
                    .addGap(24, 24, 24)
                    .addComponent(jLabel5)
                    .addGap(24, 24, 24)
                    .addComponent(jLabel3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jButtonOK)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOKActionPerformed
        Block b = new Block();
        try {
            b.setBlockAnmerkung(jTextAreaAnmkerung.getText());
            Blockleiter blockleiter = (Blockleiter) ((DefaultListModel) jListBlockleiter.getModel()).get(jListBlockleiter.getSelectedIndex());
            b.setFkBlockleiter(blockleiter.getIdBlockleiter());
            b.setFkKurs(this.idKurs);
            b.setMaxAnzahlTeilnehmer(Integer.parseInt(jTextFieldMaxTeilnehmer.getText()));
            b.setMinAnzahlTeilnehmer(Integer.parseInt(jTextFieldMinTeilnehmer.getText()));
            String mitglied = jFormattedTextFieldMitgliedspreis.getText().replace(',', '.');
            b.setMitgliedspreis(Float.parseFloat(mitglied));
            String preis = jFormattedTextFieldPreis.getText().replace(',', '.');
            b.setPreis(Float.parseFloat(preis));            b.setSemester((Semester) jSpinnerSemester.getValue());
            Connection c = User.getConnectionFromUser();
            PreparedStatement new_block = c.prepareStatement(
                    "INSERT INTO ekiz.block (MaxAnzahlTeilnehmer, Semester, fkBlockleiter, fkKurs, Preis, Mitgliedspreis,"
                    + " MinAnzahlTeilnehmer, BlockAnmerkungen) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
            );
            new_block.setInt(1, b.getMaxAnzahlTeilnehmer());
            new_block.setString(2, b.getSemester().toString());
            new_block.setInt(3, b.getFkBlockleiter());
            new_block.setInt(4, b.getFkKurs());
            new_block.setDouble(5, b.getPreis());
            new_block.setDouble(6, b.getMitgliedspreis());
            new_block.setInt(7, b.getMinAnzahlTeilnehmer());
            new_block.setString(8, b.getBlockAnmerkung());
            new_block.execute();
            c.commit();
            this.setVisible(false);
        } catch (IndexOutOfBoundsException iex) {
            JOptionPane.showMessageDialog(this, "Es muss ein Blockleiter ausgewählt werden");
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Sie müssen Zahlen eingeben");
        } catch (NullPointerException nex) {
            JOptionPane.showMessageDialog(this, "Sie haben einen Wert ausgelassen");
        } catch (SQLException ex) {
            Logger.getLogger(AddBlockZuKurs.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            User.closeConnection();
        }

    }//GEN-LAST:event_jButtonOKActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddBlockZuKurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddBlockZuKurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddBlockZuKurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddBlockZuKurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                AddBlockZuKurs dialog = new AddBlockZuKurs(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        this.notify();
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonOK;
    private javax.swing.JFormattedTextField jFormattedTextFieldMitgliedspreis;
    private javax.swing.JFormattedTextField jFormattedTextFieldPreis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList<String> jListBlockleiter;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinnerSemester;
    private javax.swing.JTextArea jTextAreaAnmkerung;
    private javax.swing.JTextField jTextFieldMaxTeilnehmer;
    private javax.swing.JTextField jTextFieldMinTeilnehmer;
    // End of variables declaration//GEN-END:variables

}
