/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import UserVerwaltung.User;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *@author mdoppler
 * @author anzen
 */
public class ErweiterteSucheBlockleiter extends javax.swing.JDialog implements SucheAbstractDialog {
    ResultSet result;
    private SucheBlockleiter parentdialoge;
    private Frame parent;
    
    /**
     * Creates new form ErweiterteSucheBlockleiter
     */
    public ErweiterteSucheBlockleiter(java.awt.Frame parent, SucheBlockleiter parentdialoge , boolean modal) {
        super(parent, modal);
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
        this.parent = parent;
        this.parentdialoge=parentdialoge;        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnAbbrechenSuche = new javax.swing.JButton();
        btnSuche = new javax.swing.JButton();
        jTextFieldBlockleiterID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldNachname = new javax.swing.JTextField();
        jTextFieldVorname = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldHandynummer = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldTelefonnummer = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldEMail = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldStrasse = new javax.swing.JTextField();
        jTextFieldHausnummer = new javax.swing.JTextField();
        jTextFieldPostleitzahl = new javax.swing.JTextField();
        jTextFieldOrt = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldKompetenzbereich = new javax.swing.JTextField();
        jCheckBoxGelöscht = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Kursleiternummer:");

        btnAbbrechenSuche.setText("Abbrechen");
        btnAbbrechenSuche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbbrechenSucheActionPerformed(evt);
            }
        });

        btnSuche.setText("Suche");
        btnSuche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSucheActionPerformed(evt);
            }
        });

        jLabel2.setText("Nachname:");

        jLabel3.setText("Vorname:");

        jLabel4.setText("Handynummer enthält:");

        jLabel5.setText("Telefonnummer enthält:");

        jLabel6.setText("E-Mail:");

        jLabel7.setText("Straße:");

        jLabel8.setText("Hausnummer:");

        jLabel9.setText("Postleitzahl:");

        jLabel10.setText("Ort:");

        jLabel11.setText("Kompetenzbereich:");

        jCheckBoxGelöscht.setText("Suche nach gelöschten Blockleitern");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(621, Short.MAX_VALUE)
                .addComponent(btnSuche)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel11)
                            .addGap(590, 590, 590))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnAbbrechenSuche)
                                    .addGap(30, 30, 30)
                                    .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(40, 40, 40)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldBlockleiterID)
                                .addComponent(jTextFieldNachname)
                                .addComponent(jTextFieldVorname)
                                .addComponent(jTextFieldHandynummer)
                                .addComponent(jTextFieldTelefonnummer)
                                .addComponent(jTextFieldEMail)
                                .addComponent(jTextFieldKompetenzbereich, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(25, 25, 25)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel9)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel10)
                                        .addComponent(jLabel7))
                                    .addGap(40, 40, 40)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldStrasse)
                                        .addComponent(jTextFieldPostleitzahl)
                                        .addComponent(jTextFieldHausnummer)
                                        .addComponent(jTextFieldOrt)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(18, 18, 18)
                                    .addComponent(jCheckBoxGelöscht)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addContainerGap()))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(266, Short.MAX_VALUE)
                .addComponent(btnSuche)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextFieldBlockleiterID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)
                        .addComponent(jTextFieldStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jTextFieldNachname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)
                        .addComponent(jTextFieldHausnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldVorname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel9)
                        .addComponent(jTextFieldPostleitzahl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jTextFieldHandynummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(jTextFieldOrt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jTextFieldTelefonnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jCheckBoxGelöscht))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jTextFieldEMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(jTextFieldKompetenzbereich, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnAbbrechenSuche, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)))
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSucheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSucheActionPerformed
        // TODO add your handling code here:       
        Integer blockleiterid = -1;
        if (!jTextFieldBlockleiterID.getText().equals("")) {
            blockleiterid = Integer.parseInt(jTextFieldBlockleiterID.getText());
        }
        String addToSQLString = "";
        ArrayList<Object[]> addToSQLDataBlockleiter = new <Object[]> ArrayList();

        String kompetenz = "%" + jTextFieldKompetenzbereich.getText() + "%";
        String handy = "%" + jTextFieldHandynummer.getText() + "%";
        String telef = "%" + jTextFieldTelefonnummer.getText() + "%";
        String mail = "%" + jTextFieldEMail.getText() + "%";

        String vn = "%" + jTextFieldVorname.getText() + "%";
        String nn = "%" + jTextFieldNachname.getText() + "%";

        String ort = "%" + jTextFieldOrt.getText() + "%";
        String plz = "%" + jTextFieldPostleitzahl.getText() + "%";
        String hausnummer = "%" + jTextFieldHausnummer.getText() + "%";
        String strasse = "%" + jTextFieldStrasse.getText() + "%";
        
        Integer gelöschteBlockleiter;
        if (jCheckBoxGelöscht.isSelected()) {
            gelöschteBlockleiter = 1;
        } else {
            gelöschteBlockleiter = 0;
        }
        
        if (blockleiterid >= 0) {
            addToSQLString += "idblockleiter = ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{blockleiterid.getClass().getName(), blockleiterid});
        }
        if (!kompetenz.equals("%%")) {
            addToSQLString += "kompetenzbereiche LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{kompetenz.getClass().getName(), kompetenz});
        }
        if (!handy.equals("%%")) {
            addToSQLString += "Handynummer LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{handy.getClass().getName(), handy});
        }
        if (!telef.equals("%%")) {
            addToSQLString += "Festnetznummer LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{telef.getClass().getName(), telef});
        }
        if (!mail.equals("%%")) {
            addToSQLString += "EMail LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{mail.getClass().getName(), mail});
        }
        if (!vn.equals("%%")) {
            addToSQLString += "Vorname LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{vn.getClass().getName(), vn});
        }
        if (!nn.equals("%%")) {
            addToSQLString += "Nachname LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{nn.getClass().getName(), nn});
        }
        if (!ort.equals("%%")) {
            addToSQLString += "Wohnort LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{ort.getClass().getName(), ort});
        }
        if (!plz.equals("%%")) {
            addToSQLString += "PLZ LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{plz.getClass().getName(), plz});
        }
        if (!hausnummer.equals("%%")) {
            addToSQLString += "HausNr LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{hausnummer.getClass().getName(), hausnummer});
        }
        if (!strasse.equals("%%")) {
            addToSQLString += "Strasse LIKE ? AND ";
            addToSQLDataBlockleiter.add(new Object[]{strasse.getClass().getName(), strasse});
        }

        try{
        Connection c= User.getConnectionFromUser();
       
            String sql = "Select * from blockleiter Join adresse on adresse.idAdresse = blockleiter.fkAdresse";

            sql += " WHERE " + addToSQLString + " BlockleiterGelöscht = b'"+gelöschteBlockleiter+"' AND";
            

            if (sql.contains("AND")) {
                sql = sql.substring(0, sql.lastIndexOf("AND"));
            }

            PreparedStatement pre = c.prepareStatement(sql);

            for (int i = 0; i < addToSQLDataBlockleiter.size(); i++) {
                Object[] oArr = addToSQLDataBlockleiter.get(i);
                try {
                    if (String.class == Class.forName(oArr[0].toString())) {
                        pre.setString(i + 1, oArr[1].toString());
                    } else {
                        pre.setInt(i + 1, Integer.parseInt(oArr[1].toString()));
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            result = pre.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       if(parentdialoge != null){ 
            parentdialoge.result = result;
            parentdialoge.dispose();
       } 
        
       
        dispose();
    }//GEN-LAST:event_btnSucheActionPerformed

    private void btnAbbrechenSucheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbbrechenSucheActionPerformed
        if(parentdialoge != null) parentdialoge.dispose();
        dispose();
    }//GEN-LAST:event_btnAbbrechenSucheActionPerformed

    @Override
    public ResultSet[] showDialog() {
       this.setVisible(true);
       if(result != null){
            return new ResultSet[]{result};
        }
        return null;
    }

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ErweiterteSucheBlockleiter dialog = new ErweiterteSucheBlockleiter(new javax.swing.JFrame(), null, true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbbrechenSuche;
    private javax.swing.JButton btnSuche;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JCheckBox jCheckBoxGelöscht;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldBlockleiterID;
    private javax.swing.JTextField jTextFieldEMail;
    private javax.swing.JTextField jTextFieldHandynummer;
    private javax.swing.JTextField jTextFieldHausnummer;
    private javax.swing.JTextField jTextFieldKompetenzbereich;
    private javax.swing.JTextField jTextFieldNachname;
    private javax.swing.JTextField jTextFieldOrt;
    private javax.swing.JTextField jTextFieldPostleitzahl;
    private javax.swing.JTextField jTextFieldStrasse;
    private javax.swing.JTextField jTextFieldTelefonnummer;
    private javax.swing.JTextField jTextFieldVorname;
    // End of variables declaration//GEN-END:variables

    
}
