/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import UserVerwaltung.User;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author anzen
 * @author mdoppler
 */
public class ErweiterteSucheErwachsener extends javax.swing.JDialog implements SucheAbstractDialog {

    ResultSet[] result;
    private SucheErwachsener parentdialoge;
    private Frame parent;

    /**
     * Creates new form ErweiterteSucheErwachsener
     */
    public ErweiterteSucheErwachsener(java.awt.Frame parent, SucheErwachsener parentDia, boolean modal) {
        super(parent, modal);
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
        this.parent = parent;
        this.parentdialoge = parentDia;


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButtonAbbrechenSucheErwachsener = new javax.swing.JButton();
        jButtonSucheErwachsener = new javax.swing.JButton();
        jTextFieldErwachsenerID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldNachname = new javax.swing.JTextField();
        jTextFieldVorname = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldHandy = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldTelefon = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldEMail = new javax.swing.JTextField();
        jCheckBoxEingezahlt = new javax.swing.JCheckBox();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldStrasse = new javax.swing.JTextField();
        jTextFieldHausnummer = new javax.swing.JTextField();
        jTextFieldPostleitzahl = new javax.swing.JTextField();
        jTextFieldOrt = new javax.swing.JTextField();
        jCheckBoxGelöscht = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Personennummer:");

        jButtonAbbrechenSucheErwachsener.setText("Abbrechen");
        jButtonAbbrechenSucheErwachsener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAbbrechenSucheErwachsenerActionPerformed(evt);
            }
        });

        jButtonSucheErwachsener.setText("Suche");
        jButtonSucheErwachsener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSucheErwachsenerActionPerformed(evt);
            }
        });

        jLabel2.setText("Nachname:");

        jLabel3.setText("Vorname:");

        jLabel4.setText("Handynummer enthält:");

        jLabel5.setText("Telefonnummer enthält:");

        jLabel6.setText("EMail:");

        jCheckBoxEingezahlt.setText("Mitgliedschaft eingezahlt");

        jLabel7.setText("Straße:");

        jLabel8.setText("Hausnummer:");

        jLabel9.setText("Postleitzahl:");

        jLabel10.setText("Ort:");

        jCheckBoxGelöscht.setText("Suche nach gelöschten Erwachsenen");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 687, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)
                        .addComponent(jLabel5)
                        .addComponent(jLabel6)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jButtonAbbrechenSucheErwachsener)
                            .addGap(30, 30, 30)
                            .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldErwachsenerID)
                                .addComponent(jTextFieldNachname)
                                .addComponent(jTextFieldVorname)
                                .addComponent(jTextFieldHandy)
                                .addComponent(jTextFieldTelefon)
                                .addComponent(jTextFieldEMail)
                                .addComponent(jCheckBoxEingezahlt, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(25, 25, 25)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel9)
                                        .addComponent(jLabel10))
                                    .addGap(40, 40, 40)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldOrt)
                                        .addComponent(jTextFieldPostleitzahl)
                                        .addComponent(jTextFieldHausnummer)
                                        .addComponent(jTextFieldStrasse)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jCheckBoxGelöscht)
                                    .addGap(0, 0, Short.MAX_VALUE))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(168, 491, Short.MAX_VALUE)
                            .addComponent(jButtonSucheErwachsener)))
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 280, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextFieldErwachsenerID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)
                        .addComponent(jTextFieldStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jTextFieldNachname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8)
                        .addComponent(jTextFieldHausnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldVorname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel9)
                        .addComponent(jTextFieldPostleitzahl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jTextFieldHandy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(jTextFieldOrt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jTextFieldTelefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jCheckBoxGelöscht))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jTextFieldEMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jCheckBoxEingezahlt)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonAbbrechenSucheErwachsener)
                            .addComponent(jButtonSucheErwachsener))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)))
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSucheErwachsenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSucheErwachsenerActionPerformed

        Integer erwachsenerid = -1;
        if (!jTextFieldErwachsenerID.getText().equals("")) {
            erwachsenerid = Integer.parseInt(jTextFieldErwachsenerID.getText());
        }
        String addToSQLString = "";
        ArrayList<Object[]> addToSQLDataErwachsener = new <Object[]> ArrayList();

        String handy = "%" + jTextFieldHandy.getText() + "%";
        String telef = "%" + jTextFieldTelefon.getText() + "%";
        String mail = "%" + jTextFieldEMail.getText() + "%";

        String vn = "%" + jTextFieldVorname.getText() + "%";
        String nn = "%" + jTextFieldNachname.getText() + "%";

        String ort = "%" + jTextFieldOrt.getText() + "%";
        String plz = "%" + jTextFieldPostleitzahl.getText() + "%";
        String hausnummer = "%" + jTextFieldHausnummer.getText() + "%";
        String strasse = "%" + jTextFieldStrasse.getText() + "%";

        Integer gelöschteErwachsene;
        if (jCheckBoxGelöscht.isSelected()) {
            gelöschteErwachsene = 1;
        } else {
            gelöschteErwachsene = 0;
        }

        if (erwachsenerid >= 0) {
            addToSQLString += "idErwachsener = ? AND ";
            addToSQLDataErwachsener.add(new Object[]{erwachsenerid.getClass().getName(), erwachsenerid});
        }
        if (!handy.equals("%%")) {
            addToSQLString += "Handynummer LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{handy.getClass().getName(), handy});
        }
        if (!telef.equals("%%")) {
            addToSQLString += "Festnetz LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{telef.getClass().getName(), telef});
        }
        if (!mail.equals("%%")) {
            addToSQLString += "EMail LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{mail.getClass().getName(), mail});
        }
        if (!vn.equals("%%")) {
            addToSQLString += "Vorname LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{vn.getClass().getName(), vn});
        }
        if (!nn.equals("%%")) {
            addToSQLString += "Nachname LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{nn.getClass().getName(), nn});
        }
        if (!ort.equals("%%")) {
            addToSQLString += "Wohnort LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{ort.getClass().getName(), ort});
        }
        if (!plz.equals("%%")) {
            addToSQLString += "PLZ LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{plz.getClass().getName(), plz});
        }
        if (!hausnummer.equals("%%")) {
            addToSQLString += "HausNr LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{hausnummer.getClass().getName(), hausnummer});
        }
        if (!strasse.equals("%%")) {
            addToSQLString += "Strasse LIKE ? AND ";
            addToSQLDataErwachsener.add(new Object[]{strasse.getClass().getName(), strasse});
        }

        try {
            Connection c = User.getConnectionFromUser();
            String sql = "Select * from erwachsener"
                    + " Join adresse on adresse.idAdresse = erwachsener.fkAdresse";
            String sqlKinder = "Select e.idErwachsener, k.idKind, k.Vorname, k.Nachname, k.Geburtsdatum, k.KindNotiz from erwachsener e"
                    + " Join erwachsener_has_kind on e.idErwachsener = erwachsener_has_kind.Erwachsener_idErwachsener"
                    + " Join kind k on k.idkind = erwachsener_has_kind.Kind_idKind";

            sql = sql + " WHERE " + addToSQLString + "ErwachsenerGelöscht = b'" + gelöschteErwachsene + "' AND";;
            if (jCheckBoxEingezahlt.isSelected()) {
                sql = sql + " current_date() <= erwachsener.Mitgliedschaft_eingezahlt";
            }

            //Nur nötig wen Eingezahlt nich Selektiert da die Behandlung von Eingezahlt scho AND wegnimmt
            if (sql.contains("AND") && !jCheckBoxEingezahlt.isSelected()) {
                sql = sql.substring(0, sql.lastIndexOf("AND"));
            }

            PreparedStatement preErwachsener = c.prepareStatement(sql);

            for (int i = 0; i < addToSQLDataErwachsener.size(); i++) {
                Object[] oArr = addToSQLDataErwachsener.get(i);
                try {
                    if (String.class == Class.forName(oArr[0].toString())) {
                        preErwachsener.setString(i + 1, oArr[1].toString());
                    } else {
                        preErwachsener.setInt(i + 1, Integer.parseInt(oArr[1].toString()));
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            result = new ResultSet[2];
            result[0] = preErwachsener.executeQuery();
            result[0].last();
            int size = result[0].getRow();
            result[0].beforeFirst(); //setzen vor erstes Element

            if (size != 0) { //Keine Erwachsenen
                sqlKinder = sqlKinder + " Where iderwachsener IN(";
                while (result[0].next()) {
                    if (!result[0].isLast()) {
                        sqlKinder = sqlKinder + result[0].getInt("idErwachsener") + ", ";
                    } else {
                        sqlKinder = sqlKinder + result[0].getInt("idErwachsener");
                    }
                }

                sqlKinder = sqlKinder + " )";
                PreparedStatement preKind = c.prepareStatement(sqlKinder);
                result[1] = preKind.executeQuery();
            }

            result[0].beforeFirst(); //setzen vor erstes Element
            

        } catch (SQLException ex) {
            Logger.getLogger(ErweiterteSucheBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (parentdialoge != null) {
            parentdialoge.result = result;
            parentdialoge.dispose();
        }
        
        
        
        dispose();

    }//GEN-LAST:event_jButtonSucheErwachsenerActionPerformed

    private void jButtonAbbrechenSucheErwachsenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAbbrechenSucheErwachsenerActionPerformed
        // TODO add your handling code here:
        if (parentdialoge != null) {
            parentdialoge.dispose();
        }
        dispose();
    }//GEN-LAST:event_jButtonAbbrechenSucheErwachsenerActionPerformed

    @Override
    public ResultSet[] showDialog() {
        this.setVisible(true);
        return result;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheErwachsener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheErwachsener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheErwachsener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ErweiterteSucheErwachsener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ErweiterteSucheErwachsener dialog = new ErweiterteSucheErwachsener(new javax.swing.JFrame(), null, true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler1;
    private javax.swing.JButton jButtonAbbrechenSucheErwachsener;
    private javax.swing.JButton jButtonSucheErwachsener;
    private javax.swing.JCheckBox jCheckBoxEingezahlt;
    private javax.swing.JCheckBox jCheckBoxGelöscht;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldEMail;
    private javax.swing.JTextField jTextFieldErwachsenerID;
    private javax.swing.JTextField jTextFieldHandy;
    private javax.swing.JTextField jTextFieldHausnummer;
    private javax.swing.JTextField jTextFieldNachname;
    private javax.swing.JTextField jTextFieldOrt;
    private javax.swing.JTextField jTextFieldPostleitzahl;
    private javax.swing.JTextField jTextFieldStrasse;
    private javax.swing.JTextField jTextFieldTelefon;
    private javax.swing.JTextField jTextFieldVorname;
    // End of variables declaration//GEN-END:variables

}
