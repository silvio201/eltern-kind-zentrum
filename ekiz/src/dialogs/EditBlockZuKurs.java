/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import GUI_MainFrame.MainFrame;
import Objects.Anrede;
import Objects.Block;
import Objects.Blockleiter;
import Objects.Semester;
import UserVerwaltung.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerListModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author lorenz
 */
public class EditBlockZuKurs extends javax.swing.JDialog {

    /**
     * Creates new form AddBlockZuKurs
     */
    private Block b;

    public EditBlockZuKurs(MainFrame parent, Block b, boolean modal) {
        super(parent, "Bearbeiten von Block", modal);
        initComponents();
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.b = b;
        setData(b);
    }

    private void setData(Block prev_block) {
        DefaultListModel dlm_blockleiter = null;
        try {
            ResultSet rs_blockleiter_list = User.getConnectionFromUser().createStatement().executeQuery("Select * from Blockleiter Order by idBlockleiter ");
            dlm_blockleiter = new DefaultListModel<Blockleiter>();
            while (rs_blockleiter_list.next()) {
                Blockleiter bleiter = new Blockleiter(Anrede.valueOf(rs_blockleiter_list.getString("Anrede")), rs_blockleiter_list.getString("Vorname"), rs_blockleiter_list.getString("nachname"), -1);
                bleiter.setIdBlockleiter(rs_blockleiter_list.getInt("idBlockleiter"));
                dlm_blockleiter.addElement(bleiter);
            }
            jListEditBlockleiter.setModel(dlm_blockleiter);
            jSpinnerEditSemester.setModel(new SpinnerListModel(Semester.values())); //jspinner 
            if (prev_block != null) {
                ResultSet rs_prev_blockleiter = User.getConnectionFromUser().createStatement().executeQuery(
                        "Select * from Blockleiter where idBlockleiter=" + prev_block.getFkBlockleiter() + " Order by idBlockleiter ");
                rs_prev_blockleiter.next();
                Blockleiter prev_blockleiter = new Blockleiter();
                prev_blockleiter.setIdBlockleiter(rs_prev_blockleiter.getInt("idBlockleiter"));
                int curr = 0;
                for (int i = 1; i < dlm_blockleiter.getSize(); i++) { //ermitteln des letzten blockleiters
                    curr = ((Blockleiter) dlm_blockleiter.getElementAt(i)).getIdBlockleiter();
                    if (curr == prev_block.getFkBlockleiter()) {
                        curr = i;
                        break;
                    }
                }

                jTextFieldEditMinTeilnehmer.setText("" + prev_block.getMinAnzahlTeilnehmer());
                jTextFieldEditMaxTeilnehmer.setText("" + prev_block.getMaxAnzahlTeilnehmer());
                jFormattedTextFieldEditPreis.setValue(prev_block.getPreis());
                jFormattedTextFieldEditMitgliedspreis.setValue(prev_block.getMitgliedspreis());
                jListEditBlockleiter.setSelectedIndex(curr);
                jSpinnerEditSemester.setValue(prev_block.getSemester());
                jTextAreaEditAnmkerung.setText(prev_block.getBlockAnmerkung());
            }
            if (LocalDate.now().isBefore(LocalDate.of(LocalDate.now().getYear(), 4, 20)) && LocalDate.now().isAfter(LocalDate.of(LocalDate.now().getYear() - 1, 9, 1))) {
                jSpinnerEditSemester.setValue(Semester.FP);
            } else {
                jSpinnerEditSemester.setValue(Semester.HP);
            }

        } catch (SQLException ex) {
            Logger.getLogger(EditBlockZuKurs.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ((DefaultEditor) jSpinnerEditSemester.getEditor()).getTextField().setEditable(false);
            User.closeConnection();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldEditMaxTeilnehmer = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSpinnerEditSemester = new javax.swing.JSpinner();
        jFormattedTextFieldEditPreis = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jFormattedTextFieldEditMitgliedspreis = new javax.swing.JFormattedTextField();
        jTextFieldEditMinTeilnehmer = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButtonEditOK = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListEditBlockleiter = new javax.swing.JList<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaEditAnmkerung = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Teilnehmer");

        jTextFieldEditMaxTeilnehmer.setMinimumSize(new java.awt.Dimension(60, 29));

        jLabel2.setText("Semester");

        jLabel3.setText("Kursleiter:");

        jLabel4.setText("Preis");

        jFormattedTextFieldEditPreis.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));

        jLabel5.setText("Mitgliedspreis");

        jFormattedTextFieldEditMitgliedspreis.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));

        jTextFieldEditMinTeilnehmer.setMinimumSize(new java.awt.Dimension(60, 29));

        jLabel6.setText("bis");

        jLabel7.setText("von");

        jButtonEditOK.setBackground(new java.awt.Color(255, 255, 255));
        jButtonEditOK.setText("OK");
        jButtonEditOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditOKActionPerformed(evt);
            }
        });

        jLabel8.setText("Anmerkung");

        jListEditBlockleiter.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListEditBlockleiter.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jListEditBlockleiter);

        jTextAreaEditAnmkerung.setColumns(20);
        jTextAreaEditAnmkerung.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jTextAreaEditAnmkerung.setRows(5);
        jScrollPane1.setViewportView(jTextAreaEditAnmkerung);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(240, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSpinnerEditSemester, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
                            .addComponent(jFormattedTextFieldEditPreis, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jFormattedTextFieldEditMitgliedspreis, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                                    .addComponent(jLabel7))
                                .addComponent(jScrollPane2))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextFieldEditMinTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(6, 6, 6)
                            .addComponent(jTextFieldEditMaxTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(14, 14, 14))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(292, 292, 292))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(113, 113, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 333, Short.MAX_VALUE)
                            .addComponent(jButtonEditOK, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jSpinnerEditSemester, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFormattedTextFieldEditPreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jFormattedTextFieldEditMitgliedspreis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextFieldEditMaxTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldEditMinTeilnehmer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jLabel2)
                    .addGap(27, 27, 27)
                    .addComponent(jLabel4)
                    .addGap(24, 24, 24)
                    .addComponent(jLabel5)
                    .addGap(24, 24, 24)
                    .addComponent(jLabel3)
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addGap(18, 18, 18)
                    .addComponent(jButtonEditOK)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonEditOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditOKActionPerformed
        try {
            b.setBlockAnmerkung(jTextAreaEditAnmkerung.getText());
            Blockleiter blockleiter = (Blockleiter) ((DefaultListModel) jListEditBlockleiter.getModel()).get(jListEditBlockleiter.getSelectedIndex());
            b.setFkBlockleiter(blockleiter.getIdBlockleiter());
            b.setMaxAnzahlTeilnehmer(Integer.parseInt(jTextFieldEditMaxTeilnehmer.getText()));
            b.setMinAnzahlTeilnehmer(Integer.parseInt(jTextFieldEditMinTeilnehmer.getText()));
            String mitglied = jFormattedTextFieldEditMitgliedspreis.getText().replace(',', '.');
            b.setMitgliedspreis(Float.parseFloat(mitglied));
            String preis = jFormattedTextFieldEditPreis.getText().replace(',', '.');
            b.setPreis(Float.parseFloat(preis));
            b.setSemester((Semester) jSpinnerEditSemester.getValue());
            Connection c = User.getConnectionFromUser();
            PreparedStatement new_block = c.prepareStatement(
                    "UPDATE ekiz.block SET MaxAnzahlTeilnehmer = ?, MinAnzahlTeilnehmer = ?, Semester = ?, fkBlockleiter = ?, Preis = ?, Mitgliedspreis = ?, BlockAnmerkungen = ? WHERE idBlock = ?"
            );
            new_block.setInt(1, b.getMaxAnzahlTeilnehmer());
            new_block.setInt(2, b.getMinAnzahlTeilnehmer());
            new_block.setString(3, b.getSemester().toString());
            new_block.setInt(4, b.getFkBlockleiter());
            new_block.setDouble(5, b.getPreis());
            new_block.setDouble(6, b.getMitgliedspreis());
            new_block.setString(7, b.getBlockAnmerkung());
            new_block.setInt(8, b.getIdBlock());
            new_block.executeUpdate();
            c.commit();
            this.setVisible(false);
        } catch (IndexOutOfBoundsException iex) {
            JOptionPane.showMessageDialog(this, "Es muss ein Blockleiter ausgewählt werden");
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Sie müssen Zahlen eingeben");
        } catch (NullPointerException nex) {
            JOptionPane.showMessageDialog(this, "Sie haben einen Wert ausgelassen");
        } catch (SQLException ex) {
            Logger.getLogger(EditBlockZuKurs.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            User.closeConnection();
        }

    }//GEN-LAST:event_jButtonEditOKActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEditOK;
    private javax.swing.JFormattedTextField jFormattedTextFieldEditMitgliedspreis;
    private javax.swing.JFormattedTextField jFormattedTextFieldEditPreis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList<String> jListEditBlockleiter;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinnerEditSemester;
    private javax.swing.JTextArea jTextAreaEditAnmkerung;
    private javax.swing.JTextField jTextFieldEditMaxTeilnehmer;
    private javax.swing.JTextField jTextFieldEditMinTeilnehmer;
    // End of variables declaration//GEN-END:variables

}
