/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import Objects.Adresse;
import Objects.Anrede;
import Objects.Blockleiter;
import UserVerwaltung.User;
import exceptions.AdresseFailed;
import exceptions.BlockleiterFailed;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author anzen
 */
public class AddBlockleiter extends javax.swing.JDialog {
    Frame parent;

    /**
     * Creates new form ErweiterteSucheBlockleiter
     */
    public AddBlockleiter(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.parent = parent;
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonAbbrechen = new javax.swing.JButton();
        jButtonHinzufuegen = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldTitel = new javax.swing.JTextField();
        jTextFieldVorname = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldHandy = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldTelefon = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldEMail = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldStrasse = new javax.swing.JTextField();
        jTextFieldHausnummer = new javax.swing.JTextField();
        jTextFieldPostleitzahl = new javax.swing.JTextField();
        jTextFieldOrt = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldKompetenz = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldNachname = new javax.swing.JTextField();
        jComboBoxAnrede = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaHonorierung = new javax.swing.JTextArea();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaKursleiter = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldLand = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextFieldStiege = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextFieldWohnung = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jButtonAbbrechen.setBackground(new java.awt.Color(255, 255, 255));
        jButtonAbbrechen.setText("Abbrechen");
        jButtonAbbrechen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAbbrechenActionPerformed(evt);
            }
        });

        jButtonHinzufuegen.setBackground(new java.awt.Color(255, 255, 255));
        jButtonHinzufuegen.setText("Hinzufügen");
        jButtonHinzufuegen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHinzufuegenActionPerformed(evt);
            }
        });

        jLabel2.setText("Nachname:");

        jLabel3.setText("Vorname:");

        jLabel4.setText("Handynummer:");

        jLabel5.setText("Telefonnummer:");

        jLabel6.setText("EMail:");

        jLabel7.setText("Straße:");

        jLabel8.setText("Hausnummer:");

        jLabel9.setText("Postleitzahl:");

        jLabel10.setText("Ort:");

        jLabel11.setText("Kompetenzbereich:");

        jLabel12.setText("Titel:");

        jLabel13.setText("Anrede:");

        jComboBoxAnrede.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Herr", "Frau", "Divers" }));

        jLabel1.setText("Anmerkung zur Honorierung:");

        jTextAreaHonorierung.setColumns(20);
        jTextAreaHonorierung.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jTextAreaHonorierung.setRows(5);
        jScrollPane1.setViewportView(jTextAreaHonorierung);

        jLabel14.setText("Anmerkung zum Kursleiter:");

        jTextAreaKursleiter.setColumns(20);
        jTextAreaKursleiter.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jTextAreaKursleiter.setRows(5);
        jScrollPane2.setViewportView(jTextAreaKursleiter);

        jLabel15.setText("Land:");

        jLabel16.setText("Stiege:");

        jLabel17.setText("Wohnungs Nummer:");

        jLabel18.setText("Bei leerem Feld: Österreich");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 723, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6)
                                .addComponent(jLabel11))
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jButtonAbbrechen)
                                    .addGap(30, 30, 30)
                                    .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel2)
                                .addComponent(jLabel12)
                                .addComponent(jLabel13)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4)
                                .addComponent(jLabel1)
                                .addComponent(jLabel14))
                            .addGap(40, 40, 40)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldHandy)
                                        .addComponent(jTextFieldTitel)
                                        .addComponent(jTextFieldTelefon)
                                        .addComponent(jTextFieldNachname)
                                        .addComponent(jTextFieldKompetenz)
                                        .addComponent(jTextFieldEMail)
                                        .addComponent(jTextFieldVorname)
                                        .addComponent(jComboBoxAnrede, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jButtonHinzufuegen))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGap(25, 25, 25)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel10)
                                                        .addComponent(jLabel15))
                                                    .addGap(103, 103, 103)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldLand, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldOrt, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel8)
                                                        .addComponent(jLabel16)
                                                        .addComponent(jLabel17))
                                                    .addGap(33, 33, 33)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldStiege, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldHausnummer, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldWohnung, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addComponent(jLabel7)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                                                    .addComponent(jTextFieldStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addComponent(jLabel18)
                                                    .addGap(0, 171, Short.MAX_VALUE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addComponent(jLabel9)
                                                    .addGap(72, 72, 72)
                                                    .addComponent(jTextFieldPostleitzahl, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)))))))))
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 523, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel13)
                                .addComponent(jComboBoxAnrede, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel12)
                                .addComponent(jTextFieldTitel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(jTextFieldNachname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel16))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jTextFieldVorname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel17))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jTextFieldHandy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(jTextFieldTelefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jTextFieldEMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(jTextFieldStrasse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel8)
                                .addComponent(jTextFieldHausnummer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jTextFieldStiege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(11, 11, 11)
                            .addComponent(jTextFieldWohnung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9)
                                .addComponent(jTextFieldPostleitzahl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(jTextFieldOrt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel15)
                                .addComponent(jTextFieldLand, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel11)
                                .addComponent(jTextFieldKompetenz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(jLabel18)))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 69, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(90, 90, 90)
                            .addComponent(jLabel14)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButtonAbbrechen)
                                    .addComponent(jButtonHinzufuegen))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(8, 8, 8)))))
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonHinzufuegenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHinzufuegenActionPerformed
        try{                                                   
            Connection connection = User.getConnectionFromUser();
            connection.setAutoCommit(false);
            try{
                Adresse adresse = getDataIntoAdresse(connection);
                getDataIntoBlockleiter(adresse, connection);
                connection.commit();
                connection.close();
                this.setVisible(false);
                this.dispose();
            }catch(AdresseFailed e){
                connection.rollback();
                JOptionPane.showMessageDialog(this, e.getMessage());
            } catch (BlockleiterFailed ex) {
                connection.rollback();
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
        }catch(SQLException ex){
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null,ex);
        }
    }//GEN-LAST:event_jButtonHinzufuegenActionPerformed

    private void jButtonAbbrechenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAbbrechenActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_jButtonAbbrechenActionPerformed
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddBlockleiter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                AddBlockleiter dialog = new AddBlockleiter(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler1;
    private javax.swing.JButton jButtonAbbrechen;
    private javax.swing.JButton jButtonHinzufuegen;
    private javax.swing.JComboBox<String> jComboBoxAnrede;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaHonorierung;
    private javax.swing.JTextArea jTextAreaKursleiter;
    private javax.swing.JTextField jTextFieldEMail;
    private javax.swing.JTextField jTextFieldHandy;
    private javax.swing.JTextField jTextFieldHausnummer;
    private javax.swing.JTextField jTextFieldKompetenz;
    private javax.swing.JTextField jTextFieldLand;
    private javax.swing.JTextField jTextFieldNachname;
    private javax.swing.JTextField jTextFieldOrt;
    private javax.swing.JTextField jTextFieldPostleitzahl;
    private javax.swing.JTextField jTextFieldStiege;
    private javax.swing.JTextField jTextFieldStrasse;
    private javax.swing.JTextField jTextFieldTelefon;
    private javax.swing.JTextField jTextFieldTitel;
    private javax.swing.JTextField jTextFieldVorname;
    private javax.swing.JTextField jTextFieldWohnung;
    // End of variables declaration//GEN-END:variables

    private Adresse getDataIntoAdresse(Connection connection) throws AdresseFailed {
        try{
            Adresse adresse;
            if(!jTextFieldOrt.getText().isEmpty() && !jTextFieldPostleitzahl.getText().isEmpty()){
                adresse = new Adresse(jTextFieldOrt.getText(), Integer.parseInt(jTextFieldPostleitzahl.getText()));
            }else{
                throw new AdresseFailed("Ort und PLZ müssen einen Wert besitzen");
            }
            
            adresse.setStrasse(jTextFieldStrasse.getText());
            adresse.setHausnr(jTextFieldHausnummer.getText());
            if(!jTextFieldStiege.getText().isEmpty()){
                adresse.setStiege(Integer.parseInt(jTextFieldStiege.getText()));
            }
            if(!jTextFieldWohnung.getText().isEmpty()){
                adresse.setWohnungsnr(Integer.parseInt(jTextFieldWohnung.getText()));
            }
            adresse.setLand(jTextFieldLand.getText());
           
            
            if(adresse.getLand().isEmpty()){
                PreparedStatement stm = connection.prepareStatement("INSERT INTO ADRESSE (strasse, wohnort, plz, hausnr, stiege, wohnungsnr) VALUES (?, ?, ?, ?, ?, ?);");
                stm.setString(1, adresse.getStrasse());
                stm.setString(2, adresse.getWohnort());
                stm.setInt(3, adresse.getPlz());
                stm.setString(4, adresse.getHausnr());
                stm.setInt(5, adresse.getStiege());
                stm.setInt(6, adresse.getWohnungsnr());
                stm.execute();
            }else{
                PreparedStatement stm = connection.prepareStatement("INSERT INTO ADRESSE (strasse, wohnort, plz, hausnr, stiege, wohnungsnr, land) VALUES (?, ?, ?, ?, ?, ?, ?);");
                stm.setString(1, adresse.getStrasse());
                stm.setString(2, adresse.getWohnort());
                stm.setInt(3, adresse.getPlz());
                stm.setString(4, adresse.getHausnr());
                stm.setInt(5, adresse.getStiege());
                stm.setInt(6, adresse.getWohnungsnr());
                stm.setString(7, adresse.getLand());
                stm.execute();
            }
            
            Statement select = connection.createStatement();
            ResultSet result = select.executeQuery("SELECT LAST_INSERT_ID() FROM ADRESSE;");
            result.next();
            adresse.setIdAdresse(result.getInt(1));
            return adresse;
            
        }catch(NumberFormatException e){
            throw new AdresseFailed("Die Werte für PLZ, Stiege und Wohnungsnummer müssen eine Zahl sein");
        } catch (SQLException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void getDataIntoBlockleiter(Adresse adresse, Connection connection) throws BlockleiterFailed {
        if(jTextFieldVorname.getText().isEmpty() || jTextFieldNachname.getText().isEmpty()){
            throw new BlockleiterFailed("Vor- und Nachname müssen einen Wert haben");
        }
        try{
            Blockleiter leiter;
        if(jComboBoxAnrede.getSelectedItem().toString().equals("Herr")){
            leiter = new Blockleiter(Anrede.Herr, jTextFieldVorname.getText(), jTextFieldNachname.getText(), adresse.getIdAdresse());
        }else if(jComboBoxAnrede.getSelectedItem().toString().equals("Frau")){
            leiter = new Blockleiter(Anrede.Frau, jTextFieldVorname.getText(), jTextFieldNachname.getText(), adresse.getIdAdresse());
        }else{
            leiter = new Blockleiter(Anrede.Divers, jTextFieldVorname.getText(), jTextFieldNachname.getText(), adresse.getIdAdresse());
        }
            leiter.setTitel(jTextFieldTitel.getText());
            leiter.setHandynummer(jTextFieldHandy.getText());
            leiter.setFestnetznummer(jTextFieldTelefon.getText());
            leiter.setEmail(jTextFieldEMail.getText());
            leiter.setKompetenzbereich(jTextFieldKompetenz.getText());
            leiter.setHonorierungsanmerkung(jTextAreaHonorierung.getText());
            leiter.setBlockleiteranmerkung(jTextAreaKursleiter.getText());
            
            
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO BLOCKLEITER (anrede, titel, vorname, nachname, email, handynummer, fkAdresse, festnetznummer, honorierungsanmerkung, blockleiteranmerkung, kompetenzbereiche) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            stmt.setString(1, leiter.getAnrede().name());
            stmt.setString(2, leiter.getTitel());
            stmt.setString(3, leiter.getVorname());
            stmt.setString(4, leiter.getNachname());
            stmt.setString(5, leiter.getEmail());
            stmt.setString(6, leiter.getHandynummer());
            stmt.setInt(7, leiter.getFkAdresse());
            stmt.setString(8, leiter.getFestnetznummer());
            stmt.setString(9, leiter.getHonorierungsanmerkung());
            stmt.setString(10, leiter.getBlockleiteranmerkung());
            stmt.setString(11, leiter.getKompetenzbereich());
            stmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AddBlockleiter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
