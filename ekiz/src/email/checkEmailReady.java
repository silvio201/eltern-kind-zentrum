/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

import UserVerwaltung.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anzen
 */
public class checkEmailReady {
    public static LinkedList<Integer> check(){
        Date d = new Date(System.currentTimeMillis()+864000000);
        Date today = new Date(System.currentTimeMillis());
        try {
            Connection c = User.getConnectionFromUser();
            PreparedStatement stmt = c.prepareStatement("SELECT fkBlock FROM TERMIN WHERE TerminStart < ? AND TerminStart > ?;");
            stmt.setDate(1, d);
            stmt.setDate(2, today);
            ResultSet result = stmt.executeQuery();
            LinkedList<Integer> fkBlock = new LinkedList<>();
            while(result.next()){
                boolean b = true;
                int res = result.getInt("fkBlock");
                for(int i = 0; i < fkBlock.size(); i++){
                    if(fkBlock.get(i) == res){
                        b = false;
                    }
                }
                if(b == true){
                    fkBlock.add(res);
                }
            }
            result.close();
            stmt.close();
            LinkedList<Integer> fkBlockrichtig = new LinkedList<>();
            for(int i = 0; i < fkBlock.size(); i++){
                stmt = c.prepareStatement("SELECT idBlock FROM BLOCK WHERE idBlock = ? AND emailVersendet = b'0';");
                stmt.setInt(1, fkBlock.get(i));
                result = stmt.executeQuery();
                while(result.next()){
                    fkBlockrichtig.add(result.getInt(1));
                }
                result.close();
                stmt.close();
            }
            LinkedList<Integer> teilnehmerids = new LinkedList<>();
            for(int i = 0; i < fkBlockrichtig.size(); i++){
                stmt = c.prepareStatement("SELECT idTeilnehmer FROM TEILNEHMER WHERE Block_idBLOCK = ?;");
                stmt.setInt(1, fkBlockrichtig.get(i));
                result = stmt.executeQuery();
                while(result.next()){
                    teilnehmerids.add(result.getInt(1));
                }
            }
            return teilnehmerids;
        } catch (SQLException ex) {
            Logger.getLogger(checkEmailReady.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
