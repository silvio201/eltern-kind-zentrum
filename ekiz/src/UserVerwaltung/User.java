/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserVerwaltung;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mdopp
 */
public class User {

    static private String username;
    static private String password;
    static private Connection connection;
    static private boolean loginSuccesfull;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.connection = null;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        User.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        User.password = password;
    }

    public static Connection getConnectionFromUser() throws SQLException {
        if (connection != null) {
            closeConnection();
        }
        System.out.println("Opening Connection for user: " + username);

        //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ekiz?zeroDateTimeBehavior=convertToNull", username, password);

        connection = DriverManager.getConnection("jdbc:mysql://10.0.0.100:3306/ekiz?zeroDateTimeBehavior=convertToNull", username, password);
        connection.setAutoCommit(false);

        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
        } catch (Exception ex) {
            System.out.println("User Connection warning");
            System.out.println(""+ex.getMessage());
        }
    }

    public static void setLoginSuccesfull(boolean loginSuccesfull) {
        User.loginSuccesfull = loginSuccesfull;
    }

    public static boolean isLoginSuccesfull() {
        return loginSuccesfull;
    }

    @Override
    public String toString() {
        return username;
    }

}
