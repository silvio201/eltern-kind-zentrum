/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author anzen
 */
public class ErwachsenerFailed extends Exception {

    /**
     * Creates a new instance of <code>ErwachsenerFailed</code> without detail
     * message.
     */
    public ErwachsenerFailed() {
    }

    /**
     * Constructs an instance of <code>ErwachsenerFailed</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ErwachsenerFailed(String msg) {
        super(msg);
    }
}
