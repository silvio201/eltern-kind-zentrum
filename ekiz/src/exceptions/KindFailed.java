/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author anzen
 */
public class KindFailed extends Exception {

    /**
     * Creates a new instance of <code>KindFailed</code> without detail message.
     */
    public KindFailed() {
    }

    /**
     * Constructs an instance of <code>KindFailed</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public KindFailed(String msg) {
        super(msg);
    }
}
