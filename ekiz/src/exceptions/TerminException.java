/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author lorenz
 */
public class TerminException extends Exception{

    public TerminException() {
    }

    public TerminException(String message) {
        super(message);
    }

    public TerminException(String message, Throwable cause) {
        super(message, cause);
    }

    public TerminException(Throwable cause) {
        super(cause);
    }

    public TerminException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }    
}
