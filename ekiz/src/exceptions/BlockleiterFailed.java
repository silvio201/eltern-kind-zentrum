/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author anzen
 */
public class BlockleiterFailed extends Exception {

    /**
     * Creates a new instance of <code>BlockleiterFailed</code> without detail
     * message.
     */
    public BlockleiterFailed() {
    }

    /**
     * Constructs an instance of <code>BlockleiterFailed</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public BlockleiterFailed(String msg) {
        super(msg);
    }
}
