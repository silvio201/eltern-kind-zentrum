/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author anzen
 */
public class KursFailed extends Exception {

    /**
     * Creates a new instance of <code>KursFailed</code> without detail message.
     */
    public KursFailed() {
    }

    /**
     * Constructs an instance of <code>KursFailed</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public KursFailed(String msg) {
        super(msg);
    }
}
