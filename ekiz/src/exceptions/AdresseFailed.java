/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author anzen
 */
public class AdresseFailed extends Exception {

    /**
     * Creates a new instance of <code>AdresseFailed</code> without detail
     * message.
     */
    public AdresseFailed() {
    }

    /**
     * Constructs an instance of <code>AdresseFailed</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public AdresseFailed(String msg) {
        super(msg);
    }
}
