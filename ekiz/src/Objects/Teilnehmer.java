/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author mdopp
 */
public class Teilnehmer {
    private int idTeilnehmer;
    private int idBlock;
    private int idErwachsener;
    
  public Teilnehmer(int idTeilnehmer, int idBlock, int idErwachsener) {
        this.idTeilnehmer = idTeilnehmer;
        this.idBlock = idBlock;
        this.idErwachsener = idErwachsener;
    }

    public Teilnehmer() {
        
    }
  
    public int getIdTeilnehmer() {
        return idTeilnehmer;
    }

    public void setIdTeilnehmer(int idTeilnehmer) {
        this.idTeilnehmer = idTeilnehmer;
    }

    public int getIdBlock() {
        return idBlock;
    }

    public void setIdBlock(int idBlock) {
        this.idBlock = idBlock;
    }

    public int getIdErwachsener() {
        return idErwachsener;
    }

    public void setIdErwachsener(int idErwachsener) {
        this.idErwachsener = idErwachsener;
    }    

    @Override
    public String toString() {
        return ""+idBlock;
    }
    
    
}
