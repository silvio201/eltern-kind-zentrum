/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author anzen
 */
public class Blockleiter {

    int idBlockleiter;
    Anrede anrede;
    String titel;
    String vorname;
    String nachname;
    String email;
    String handynummer;
    int fkAdresse;
    String festnetznummer;
    String honorierungsanmerkung;
    String blockleiteranmerkung;
    String kompetenzbereich;

    public Blockleiter(Anrede anrede, String vorname, String nachname, int fkAdresse) {
        this.anrede = anrede;
        this.vorname = vorname;
        this.nachname = nachname;
        this.fkAdresse = fkAdresse;
    }

    public Blockleiter() {
    }

    public int getIdBlockleiter() {
        return idBlockleiter;
    }

    public void setIdBlockleiter(int idBlockleiter) {
        this.idBlockleiter = idBlockleiter;
    }

    public Anrede getAnrede() {
        return anrede;
    }

    public void setAnrede(Anrede anrede) {
        this.anrede = anrede;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHandynummer() {
        return handynummer;
    }

    public void setHandynummer(String handynummer) {
        this.handynummer = handynummer;
    }

    public int getFkAdresse() {
        return fkAdresse;
    }

    public void setFkAdresse(int fkAdresse) {
        this.fkAdresse = fkAdresse;
    }

    public String getFestnetznummer() {
        return festnetznummer;
    }

    public void setFestnetznummer(String festnetznummer) {
        this.festnetznummer = festnetznummer;
    }

    public String getHonorierungsanmerkung() {
        return honorierungsanmerkung;
    }

    public void setHonorierungsanmerkung(String honorierungsanmerkung) {
        this.honorierungsanmerkung = honorierungsanmerkung;
    }

    public String getBlockleiteranmerkung() {
        return blockleiteranmerkung;
    }

    public void setBlockleiteranmerkung(String blockleiteranmerkung) {
        this.blockleiteranmerkung = blockleiteranmerkung;
    }

    public String getKompetenzbereich() {
        return kompetenzbereich;
    }

    public void setKompetenzbereich(String kompetenzbereich) {
        this.kompetenzbereich = kompetenzbereich;
    }
    
    @Override
    public String toString() {
        return this.nachname + " " + this.vorname;
    }
}
