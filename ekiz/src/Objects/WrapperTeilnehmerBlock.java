/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import java.sql.Date;

/**
 *
 * @author mdopp
 */
public class WrapperTeilnehmerBlock {

    private Erwachsener e;
    private boolean bezahlt;
    private String bezahlungsNotiz;
    private String teilnehmerNotiz;
    private Teilnehmer teilnehmer;

    public WrapperTeilnehmerBlock(Erwachsener e, boolean bezahlt, String bezahlungsNotiz, String teilnehmerNotiz) {
        this.e = e;
        this.bezahlt = bezahlt;
        this.bezahlungsNotiz = bezahlungsNotiz;
        this.teilnehmerNotiz = teilnehmerNotiz;
    }

    public Erwachsener getE() {
        return e;
    }

    public void setE(Erwachsener e) {
        this.e = e;
    }

    public boolean isBezahlt() {
        return bezahlt;
    }

    public void setBezahlt(boolean bezahlt) {
        this.bezahlt = bezahlt;
    }

    public String getBezahlungsNotiz() {
        return bezahlungsNotiz;
    }

    public void setBezahlungsNotiz(String bezahlungsNotiz) {
        this.bezahlungsNotiz = bezahlungsNotiz;
    }

    public String getTeilnehmerNotiz() {
        return teilnehmerNotiz;
    }

    public void setTeilnehmerNotiz(String teilnehmerNotiz) {
        this.teilnehmerNotiz = teilnehmerNotiz;
    }

    public Teilnehmer getTeilnehmer() {
        return teilnehmer;
    }

    public void setTeilnehmer(Teilnehmer teilnehmer) {
        this.teilnehmer = teilnehmer;
    }

   
    
    
    @Override
    public String toString() {
        String line = "";
        Date d = e.mitgliedschaft_eingezhlt;
        if (d != null) {
            long date1 = d.getTime();
            long date2 = System.currentTimeMillis();
            long diff = (date2 - date1) / (24 * 60 * 60 * 1000);
            if (diff < 365) {
                line += "(M) ";
            }else{
                line+="(N) ";
            }
        }

        line += e.toString();
        if (!bezahlt) {
            line += " (Noch nicht bezahlt)";
        }
        return line;
    }

}
