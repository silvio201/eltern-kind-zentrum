/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author anzen
 */
public class Adresse {
    int idAdresse;
    String strasse;
    String wohnort;
    int plz;
    String hausnr;
    int stiege;
    int wohnungsnr;
    String land;

    public Adresse(String wohnort, int plz) {
        this.wohnort = wohnort;
        this.plz = plz;
    }

    public Adresse() {
    }
    
    

    public int getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(int idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getWohnort() {
        return wohnort;
    }

    public void setWohnort(String wohnort) {
        this.wohnort = wohnort;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getHausnr() {
        return hausnr;
    }

    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    public int getStiege() {
        return stiege;
    }

    public void setStiege(int stiege) {
        this.stiege = stiege;
    }

    public int getWohnungsnr() {
        return wohnungsnr;
    }

    public void setWohnungsnr(int wohnungsnr) {
        this.wohnungsnr = wohnungsnr;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }
    
    
}
