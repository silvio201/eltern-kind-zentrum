/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author anzen
 */
public class Kind {
    int idKind;
    String vorname;
    String nachname;
    String notiz;
    Date geburtsdatum;
    ArrayList<Erwachsener> eltern;
    boolean istGeloescht;

    public Kind() {
    }
    
    public Kind(String vorname, String nachname, Date geburtsdatum) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsdatum = geburtsdatum;
        this.eltern=new ArrayList<>();
    }

    public void addEltern(Erwachsener e) {
        eltern.add(e);
    }

    public void setEltern(ArrayList<Erwachsener> eltern) {
        this.eltern = eltern;
    }

    public ArrayList<Erwachsener> getEltern() {
        return eltern;
    }
    

    public int getIdKind() {
        return idKind;
    }

    public void setIdKind(int idKind) {
        this.idKind = idKind;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getNotiz() {
        return notiz;
    }

    public void setNotiz(String notiz) {
        this.notiz = notiz;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }
    
     public boolean isIstGeloescht() {
        return istGeloescht;
    }

    public void setIstGeloescht(boolean istGeloescht) {
        this.istGeloescht = istGeloescht;
    }
    
    @Override
    public String toString() {
        return  vorname + " " + nachname;
    }
    
    
}
