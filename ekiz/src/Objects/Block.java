/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author anzen
 */
public class Block{
    int idBlock;
    int maxAnzahlTeilnehmer;
    Semester semester;
    int fkBlockleiter;
    int fkKurs;
    double preis;
    double mitgliedspreis;
    int minAnzahlTeilnehmer;
    String blockAnmerkung;
    
    private String yearOfTerminChildren;

    public Block(int maxAnzahlTeilnehmer, Semester semester, int fkKurs, double preis, double mitgliedspreis, int minAnzahlTeilnehmer) {
        this.yearOfTerminChildren = "";
        this.maxAnzahlTeilnehmer = maxAnzahlTeilnehmer;
        this.semester = semester;
        this.fkKurs = fkKurs;
        this.preis = preis;
        this.mitgliedspreis = mitgliedspreis;
        this.minAnzahlTeilnehmer = minAnzahlTeilnehmer;
    }   
    
    public Block() {
       
    }
    

    public int getIdBlock() {
        return idBlock;
    }

    public void setIdBlock(int idBlock) {
        this.idBlock = idBlock;
    }

    public int getMaxAnzahlTeilnehmer() {
        return maxAnzahlTeilnehmer;
    }

    public void setMaxAnzahlTeilnehmer(int maxAnzahlTeilnehmer) {
        this.maxAnzahlTeilnehmer = maxAnzahlTeilnehmer;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public int getFkBlockleiter() {
        return fkBlockleiter;
    }

    public void setFkBlockleiter(int fkBlockleiter) {
        this.fkBlockleiter = fkBlockleiter;
    }

    public int getFkKurs() {
        return fkKurs;
    }

    public void setFkKurs(int fkKurs) {
        this.fkKurs = fkKurs;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public double getMitgliedspreis() {
        return mitgliedspreis;
    }

    public void setMitgliedspreis(double mitgliedspreis) {
        this.mitgliedspreis = mitgliedspreis;
    }

    public int getMinAnzahlTeilnehmer() {
        return minAnzahlTeilnehmer;
    }

    public void setMinAnzahlTeilnehmer(int minAnzahlTeilnehmer) {
        this.minAnzahlTeilnehmer = minAnzahlTeilnehmer;
    }

    public String getBlockAnmerkung() {
        return blockAnmerkung;
    }

    public void setBlockAnmerkung(String blockAnmerkung) {
        this.blockAnmerkung = blockAnmerkung;
    }

    public String getYearOfTerminChildren() {
        return yearOfTerminChildren;
    }

    public void setYearOfTerminChildren(String yearOfTerminChildren) {
        this.yearOfTerminChildren = yearOfTerminChildren;
    }

    @Override
    public String toString() {
       return semester+yearOfTerminChildren+" | "+minAnzahlTeilnehmer+"-"+maxAnzahlTeilnehmer+"| (M) "+mitgliedspreis+"€ | (N) "+preis+"€";
    }
    
    
}
