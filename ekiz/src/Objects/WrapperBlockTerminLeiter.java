/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author mdopp
 */
public class WrapperBlockTerminLeiter {
    private Block block;
    private String date;
    private String leiter;

    public WrapperBlockTerminLeiter(Block block, String date, String leiter) {
        this.block = block;
        this.date = date;
        this.leiter = leiter;
    }
    
    

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLeiter() {
        return leiter;
    }

    public void setLeiter(String leiter) {
        this.leiter = leiter;
    }
    
    @Override
    public String toString() {
        return ""+block.getSemester()+" "+leiter+" "+date;
    }
    
    
    
}
