/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import UserVerwaltung.User;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author anzen
 */
public class Erwachsener {

    int idErwachsener;
    String titel;
    Anrede anrede;
    String nachname;
    String vorname;
    String handynummer;
    String festnetz;
    String email;
    Date mitgliedschaft_eingezhlt;
    int fkAdresse;
    String firmenName;
    String erwachsenernotiz;
    boolean istGeloescht;
    boolean Programmheft;

    public Erwachsener(Anrede anrede, int fkAdresse) {
        this.anrede = anrede;
        this.fkAdresse = fkAdresse;
    }

    public Erwachsener() {
        titel = "";
        nachname = "";
        vorname = "";
        handynummer = "";
        festnetz = "";
        email = "";
        firmenName = "";
        erwachsenernotiz = "";
    }

    public Erwachsener(int idErwachsener, String nachname, String vorname) {
        this();
        this.idErwachsener = idErwachsener;
        this.nachname = nachname;
        this.vorname = vorname;
    }

    public int getIdErwachsener() {
        return idErwachsener;
    }

    public void setIdErwachsener(int idErwachsener) {
        this.idErwachsener = idErwachsener;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        if (titel != null) {
            this.titel = titel;
        }
    }

    public Anrede getAnrede() {
        return anrede;
    }

    public void setAnrede(Anrede anrede) {
        this.anrede = anrede;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        if (nachname != null) {
            this.nachname = nachname;
        }
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        if (vorname != null) {
            this.vorname = vorname;
        }
    }

    public String getHandynummer() {
        return handynummer;
    }

    public void setHandynummer(String handynummer) {
        if (handynummer != null) {
            this.handynummer = handynummer;
        }
    }

    public String getFestnetz() {
        return festnetz;
    }

    public void setFestnetz(String festnetz) {
        if (festnetz != null) {
            this.festnetz = festnetz;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email != null) {
            this.email = email;
        }
    }

    public Date getMitgliedschaft_eingezhlt() {
        return mitgliedschaft_eingezhlt;
    }

    public void setMitgliedschaft_eingezahlt(Date mitgliedschaft_eingezhlt) {
        this.mitgliedschaft_eingezhlt = mitgliedschaft_eingezhlt;
    }

    public int getFkAdresse() {
        return fkAdresse;
    }

    public void setFkAdresse(int fkAdresse) {
        this.fkAdresse = fkAdresse;
    }

    public String getFirmenName() {
        return firmenName;
    }

    public void setFirmenName(String firmenName) {
        if (firmenName != null) {
            this.firmenName = firmenName;
        }
    }

    public String getErwachsenernotiz() {
        return erwachsenernotiz;
    }

    public void setErwachsenernotiz(String erwachsenernotiz) {
        this.erwachsenernotiz = erwachsenernotiz;
    }

    public boolean isIstGeloescht() {
        return istGeloescht;
    }

    public void setIstGeloescht(boolean istGeloescht) {
        this.istGeloescht = istGeloescht;
    }

    public boolean isProgrammheft() {
        return Programmheft;
    }

    public void setProgrammheft(boolean Programmheft) {
        this.Programmheft = Programmheft;
    }
    

    @Override
    public String toString() {
        String s = "";
        if (firmenName == null || firmenName.isEmpty()) {
            s = anrede + " " + titel + " " + " " + vorname + " " + nachname;
        } else {
            s = firmenName + ": " + anrede + " " + titel + " " + " " + vorname + " " + nachname;
        }
        s = s.replace(" null ", "");
        return s;
    }

    public String toCSVformat() {
        StringBuilder strCSV = new StringBuilder();
        strCSV.append(anrede + " ");
        strCSV.append(vorname + " ");
        strCSV.append(nachname);
        strCSV.append(";");
        try {
            ResultSet rs = User.getConnectionFromUser().createStatement().executeQuery("Select Wohnort from adresse where idAdresse = " + fkAdresse);
            rs.next();
            strCSV.append(rs.getString("Wohnort")+";");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        strCSV.append(email);
        if (handynummer != null || !handynummer.equals("")) {
            strCSV.append(";");
            strCSV.append(handynummer);
        } else {
            strCSV.append(";");
            strCSV.append(festnetz);
        }
        return strCSV.toString();
    }

}
