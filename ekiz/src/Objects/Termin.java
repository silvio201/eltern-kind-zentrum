/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

import java.sql.Date;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 *
 * @author anzen
 */
public class Termin {

    int idTermin;
    Date TerminDatum;
    Time TerminStart;
    Time TerminEnde;
    int fkBlock;
    static String[] wochentage = {"Sonntag", "Montag", "Dienstag",
        "Mittwoch", "Donnerstag", "Freitag", "Samstag"};
    static String[] monate = {"Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August",
        "September", "Oktober", "November", "Dezember"};

    public Termin(Date TerminDatum, Time TerminStart, Time TerminEnde, int fkBlock, int idTermin) {
        this.TerminDatum = TerminDatum;
        this.TerminStart = TerminStart;
        this.TerminEnde = TerminEnde;
        this.fkBlock = fkBlock;
        this.idTermin=idTermin;
    }

    public Termin(Date TerminDatum, Time TerminStart, Time TerminEnde, int fkBlock) {
        this.TerminDatum = TerminDatum;
        this.TerminStart = TerminStart;
        this.TerminEnde = TerminEnde;
        this.fkBlock = fkBlock;
    }

    public int getIdTermin() {
        return idTermin;
    }

    public void setIdTermin(int idTermin) {
        this.idTermin = idTermin;
    }

    public Time getTerminStart() {
        return TerminStart;
    }

    public void setTerminStart(Time TerminStart) {
        this.TerminStart = TerminStart;
    }

    public Time getTerminEnde() {
        return TerminEnde;
    }

    public void setTerminEnde(Time TerminEnde) {
        this.TerminEnde = TerminEnde;
    }

    public int getFkBlock() {
        return fkBlock;
    }

    public void setFkBlock(int fkBlock) {
        this.fkBlock = fkBlock;
    }

    public Date getTerminDatum() {
        return TerminDatum;
    }

    public void setTerminDatum(Date TerminDatum) {
        this.TerminDatum = TerminDatum;
    }

    @Override
    public String toString() {
        LocalDate termin_datum = TerminDatum.toLocalDate();
        return termin_datum.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.GERMAN) + ", "+termin_datum.getDayOfMonth()+". " + termin_datum.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN) + " "+termin_datum.getYear()+" " + TerminStart.toString().substring(0, 5) + "-" + TerminEnde.toString().substring(0, 5);
    }

}
