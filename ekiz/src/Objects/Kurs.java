/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objects;

/**
 *
 * @author anzen
 */
public class Kurs {
    int idKurs;
    String kursname;

    public Kurs() {
    }

    public Kurs(String kursname) {
        this.kursname = kursname;
    }

    public Kurs(int idKurs) {
        this.idKurs = idKurs;
    }

    public Kurs(int idKurs, String kursname) {
        this.idKurs = idKurs;
        this.kursname = kursname;
    }

    public int getIdKurs() {
        return idKurs;
    }

    public void setIdKurs(int idKurs) {
        this.idKurs = idKurs;
    }

    public String getKursname() {
        return kursname;
    }

    public void setKursname(String kursname) {
        this.kursname = kursname;
    }

    @Override
    public String toString() {
        return idKurs+" "+kursname;
    }
    
    
    
}
