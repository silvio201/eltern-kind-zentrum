/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delete;

import GUI_MainFrame.MainFrame;
import Objects.Erwachsener;
import Objects.Kind;
import Objects.Teilnehmer;
import UserVerwaltung.User;
import java.rmi.UnexpectedException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author mdopp
 */
public class DeleteMethods {

    public static boolean kompletesLöschenErwachsener(int iderwachsener, JFrame parent) {
        Connection c = null;
        try {
            c = User.getConnectionFromUser();
            PreparedStatement preKindErwachsener = c.prepareStatement("Select Kind_idKind, vorname, nachname From erwachsener_has_kind "
                    + "JOIN Kind On kind.idKind = erwachsener_has_kind.Kind_idKind where erwachsener_iderwachsener = ?");
            preKindErwachsener.setInt(1, iderwachsener);

            //1..Alle Kinder löschen;2..Nur die nötigen Kinder löschen; 3..Löscht die Beziehung zwischen Erwachsenen und den Kindern
            int deleteMode = ueberpreufeObKindGeloeschtWerdenSoll(iderwachsener, c, parent);
            ResultSet rsKinder = null;
            switch (deleteMode) {
                case 1:
                    rsKinder = preKindErwachsener.executeQuery();
                    System.out.println("" + rsKinder.getRow());
                    while (rsKinder.next()) {
                        int idKind = rsKinder.getInt("Kind_idKind");

                        PreparedStatement preVerbindung = c.prepareStatement("Delete from erwachsener_has_kind where Kind_idKind = ?");
                        preVerbindung.setInt(1, idKind);
                        preVerbindung.executeUpdate();

                        PreparedStatement preKind = c.prepareStatement("Delete from Kind where idKind = ?");
                        preKind.setInt(1, idKind);
                        preKind.executeUpdate();

                    }
                    break;

                case 2:
                    rsKinder = preKindErwachsener.executeQuery();
                    while (rsKinder.next()) {
                        int idKind = rsKinder.getInt("Kind_idKind");

                        PreparedStatement preAndererElternteil = c.prepareStatement("Select vorname, nachname From erwachsener_has_kind "
                                + "JOIN erwachsener ON idErwachsener = Erwachsener_idErwachsener where kind_idKind = ?");
                        preAndererElternteil.setInt(1, idKind);
                        ResultSet rsAndererElternteil = preAndererElternteil.executeQuery();
                        rsAndererElternteil.last();

                        PreparedStatement preVerbindung = c.prepareStatement("Delete from erwachsener_has_kind where Kind_idKind = ? AND erwachsener_iderwachsener = ?");
                        preVerbindung.setInt(1, idKind);
                        preVerbindung.setInt(2, iderwachsener);
                        preVerbindung.executeUpdate();

                        //Zweiter Elternteil vorhanden?
                        if (rsAndererElternteil.getRow() == 1) {
                            PreparedStatement preKind = c.prepareStatement("Delete from Kind where idKind = ?");
                            preKind.setInt(1, idKind);
                            preKind.executeUpdate();
                        }
                    }
                    break;

                case 3:
                    PreparedStatement preDeleteVerbindungErwachsener = c.prepareStatement("Delete From erwachsener_has_kind where erwachsener_iderwachsener = ?");
                    preDeleteVerbindungErwachsener.setInt(1, iderwachsener);
                    preDeleteVerbindungErwachsener.executeUpdate();
                    break;

                default:
                    c.close();
                    return false;

            }

            //PreparedStatement preErwachsener = c.prepareStatement("Delete from erwachsener where idErwachsener = ?");
            PreparedStatement preErwachsener = c.prepareStatement("Select idTeilnehmer from erwachsener e\n"
                    + "join Teilnehmer t on e.iderwachsener = t.erwachsener_idErwachsener\n"
                    + "where e.iderwachsener = ?");
            preErwachsener.setInt(1, iderwachsener);
            ResultSet rsErwachsenerTeilnehmer = preErwachsener.executeQuery();

            PreparedStatement preUpdateAnonym = c.prepareStatement("Update Teilnehmer set Erwachsener_idErwachsener = ?, Bezahlung_abgeschlossen=b'1',BezahlungNotiz='Anonymer User' where idTeilnehmer = ?"); 
            PreparedStatement preIdAnonym = c.prepareStatement("Select iderwachsener from erwachsener e\n"
                    + "where e.nachname='Gelöschter ERWACHSENER' AND e.vorname='Gelöschter ERWACHSENER' AND e.anrede='Herr'");
            ResultSet rsIdAnonym = preIdAnonym.executeQuery();
            rsIdAnonym.last();

            if (rsIdAnonym.getRow() != 1) {
                throw new UnexpectedException("Mehere oder kein Anonymer User vorhanden");
            }
            int idAnonymerUser = rsIdAnonym.getInt("iderwachsener");

            while (rsErwachsenerTeilnehmer.next()) {
                preUpdateAnonym.setInt(1, idAnonymerUser);
                preUpdateAnonym.setInt(2, rsErwachsenerTeilnehmer.getInt("idTeilnehmer"));
                preUpdateAnonym.executeUpdate();
            }
            
            PreparedStatement preDeleteErwachsener = c.prepareStatement("Delete From Erwachsener where iderwachsener = ?");
            preDeleteErwachsener.setInt(1, iderwachsener);
            preDeleteErwachsener.executeUpdate();

            c.commit();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);

            return false;
        } catch (UnexpectedException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);

            return false;
        }
        return true;
    }

    private static int ueberpreufeObKindGeloeschtWerdenSoll(int iderwachsener, Connection c, JFrame parent) throws SQLException, UnexpectedException {

        PreparedStatement preKindErwachsener = c.prepareStatement("Select Kind_idKind, vorname, nachname From erwachsener_has_kind "
                + "JOIN Kind On kind.idKind = erwachsener_has_kind.Kind_idKind where erwachsener_iderwachsener = ? AND KindGelöscht = b'0'");

        preKindErwachsener.setInt(1, iderwachsener);
        ResultSet rsKinder = preKindErwachsener.executeQuery();
        rsKinder.last();

        //Keine Kinder vorhanden?
        if (rsKinder.getRow() != 0) {
            rsKinder.beforeFirst();
            String namekinder = "";
            String namekinderOhneAndereEltern = "";

            while (rsKinder.next()) {
                int idKind = rsKinder.getInt("Kind_idKind");
                namekinder += rsKinder.getNString("vorname") + " " + rsKinder.getNString("nachname") + "\n";
                PreparedStatement preAndererElternteil = c.prepareStatement("Select vorname, nachname From erwachsener_has_kind "
                        + "JOIN erwachsener ON idErwachsener = Erwachsener_idErwachsener where kind_idKind = ?");
                preAndererElternteil.setInt(1, idKind);

                ResultSet rsAndererElternteil = preAndererElternteil.executeQuery();
                rsAndererElternteil.last();
                //Zweiter Elternteil vorhanden?
                if (rsAndererElternteil.getRow() == 1) {
                    namekinderOhneAndereEltern += rsKinder.getNString("vorname") + " " + rsKinder.getNString("nachname") + "\n"; //Wenn nicht vorhanden hinzufügen von Liste
                }
            }
            rsKinder.close();

            //1..Alle Löschen; 2..Nur nötige löschen; 3..Keine Kinder löschen
            //Wen Kinder gelöscht werden müssen die keinen weiteren Elternteil mehr haben
            int answere = JOptionPane.showConfirmDialog(parent, "Wollen sie die Kinder ebenfalls löschen? \n" + namekinder, "Wollen sie die Kinder löschen", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
            if (answere == JOptionPane.YES_OPTION) {
                return 1;
            } else {

                if (!namekinderOhneAndereEltern.equals("")) {

                    answere = JOptionPane.showConfirmDialog(parent, "Folgende Kinder werden gelöscht weil kein weiterer Elternteil vorhanden ist: \n" + namekinderOhneAndereEltern, "Wollen sie die Kinder löschen", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                    if (answere == JOptionPane.YES_OPTION) {
                        return 2;
                    } else {
                        return -1;
                    }
                }
                return 3;
            }
        }
        return 1;
    }

    public static boolean löscheErwachsenerAusSuche(int iderwachsener, JFrame parent) {
        Connection c = null;
        try {
            PreparedStatement preLöscheKinder;
            ResultSet rsKinder;

            c = User.getConnectionFromUser();
            preLöscheKinder = c.prepareStatement("Update Kind Set KindGelöscht = b'1' Where idKind = ?");
            PreparedStatement preKinder = c.prepareStatement("Select Kind_idKind, vorname, nachname From erwachsener_has_kind "
                    + "JOIN Kind On kind.idKind = erwachsener_has_kind.Kind_idKind where erwachsener_iderwachsener = ?");
            preKinder.setInt(1, iderwachsener);
            rsKinder = preKinder.executeQuery();

            //1..Alle Löschen; 2..Nur nötige löschen; 3..Keine Kinder löschen
            switch (ueberpreufeObKindGeloeschtWerdenSoll(iderwachsener, c, parent)) {
                case 1:
                    while (rsKinder.next()) {
                        int idKind = rsKinder.getInt("Kind_idKind");
                        preLöscheKinder.setInt(1, idKind);
                        preLöscheKinder.executeUpdate();
                    }
                    c.commit();
                    break;
                case 2:
                    while (rsKinder.next()) {
                        int idKind = rsKinder.getInt("Kind_idKind");

                        PreparedStatement preAndererElternteil = c.prepareStatement("Select vorname, nachname From erwachsener_has_kind "
                                + "JOIN erwachsener ON idErwachsener = Erwachsener_idErwachsener where kind_idKind = ?");
                        preAndererElternteil.setInt(1, idKind);
                        ResultSet rsAndererElternteil = preAndererElternteil.executeQuery();
                        rsAndererElternteil.last();

                        //Zweiter Elternteil vorhanden?
                        if (rsAndererElternteil.getRow() == 1) {
                            preLöscheKinder.setInt(1, idKind);
                            preLöscheKinder.executeUpdate();
                        }
                    }
                    break;
                case 3:
                    c.close();
                    return false;
                default:
                    c.close();
                    return false;
                //Nachdem zweimal löschen mit nein beantwortet wurde --> return
            }

            c = User.getConnectionFromUser();
            PreparedStatement preLöscheErwachsener = c.prepareStatement("Update Erwachsener Set ErwachsenerGelöscht = b'1' Where idErwachsener = ?");
            preLöscheErwachsener.setInt(1, iderwachsener);
            preLöscheErwachsener.executeUpdate();

            c.commit();
            c.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (UnexpectedException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public static boolean löscheBlockleiterAusSuche(int idBlockleiter) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            PreparedStatement pre = c.prepareStatement("Update Blockleiter Set BlockleiterGelöscht = b'1' Where idBlockleiter = ?");
            pre.setInt(1, idBlockleiter);
            pre.executeUpdate();
            c.commit();
            c.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean löscheKinderAusSuche(int idKind) {
        try {
            Connection c;
            c = User.getConnectionFromUser();
            PreparedStatement pre = c.prepareStatement("Update Kind Set KindGelöscht = b'1' Where idKind = ?");
            pre.setInt(1, idKind);
            pre.executeUpdate();

            c.commit();
            c.close();
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean kompletesLöschenKind(int idKind) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            PreparedStatement preVerbindung = c.prepareStatement("Delete from erwachsener_has_kind where Kind_idKind = ?");
            preVerbindung.setInt(1, idKind);
            preVerbindung.executeUpdate();

            PreparedStatement preKind = c.prepareStatement("Delete from Kind where idKind = ?");
            preKind.setInt(1, idKind);
            preKind.executeUpdate();

            c.commit();
            c.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean löscheTermin(int idTermin) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            PreparedStatement preVerbindung = c.prepareStatement("Delete from Termin where idTermin = ?");
            preVerbindung.setInt(1, idTermin);
            preVerbindung.executeUpdate();

            c.commit();
            c.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean löscheBlock(int idBlock) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            teilLöscheBlock(c, idBlock);

            c.commit();
            c.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static void teilLöscheBlock(Connection c, int idBlock) {
        try {
            PreparedStatement preTermine = c.prepareStatement("Delete from Termin where\n"
                    + "fkblock=?");
            preTermine.setInt(1, idBlock);
            preTermine.executeUpdate();

            PreparedStatement preTeilnehmer = c.prepareStatement("Delete From Teilnehmer where block_idblock=?");
            preTeilnehmer.setInt(1, idBlock);
            preTeilnehmer.executeUpdate();

            PreparedStatement preDeleteBlock = c.prepareStatement("Delete from block where idblock = ?");
            preDeleteBlock.setInt(1, idBlock);
            preDeleteBlock.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean löscheKurs(int idKurs) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            PreparedStatement preKurs = c.prepareStatement("Select idBlock from Block where fkKurs = ?");
            preKurs.setInt(1, idKurs);
            ResultSet rsBlock = preKurs.executeQuery();

            while (rsBlock.next()) {
                teilLöscheBlock(c, rsBlock.getInt("idblock"));
            }

            PreparedStatement preDelKurs = c.prepareStatement("Delete From kurs where idKurs=?");
            preDelKurs.setInt(1, idKurs);
            preDelKurs.executeUpdate();

            c.commit();
            c.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean löscheTeilnehmer(int idErwachsener, int idBlock) {
        try {
            Connection c;
            c = User.getConnectionFromUser();

            PreparedStatement preVerbindung = c.prepareStatement("Delete from teilnehmer where erwachsener_iderwachsener = ? AND\n"
                    + "block_idblock = ?");
            preVerbindung.setInt(1, idErwachsener);
            preVerbindung.setInt(2, idBlock);
            preVerbindung.executeUpdate();

            c.commit();
            c.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public static boolean deleteTeilnehmer(Teilnehmer t) {

        try {
            Connection c = User.getConnectionFromUser();
            PreparedStatement pre = c.prepareStatement("Delete From Teilnehmer where Block_idBlock=? AND Erwachsener_idErwachsener=? AND idTeilnehmer=?");
            pre.setInt(1, t.getIdBlock());
            pre.setInt(2, t.getIdErwachsener());
            pre.setInt(3, t.getIdTeilnehmer());
            pre.executeUpdate();
            c.commit();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static boolean deleteErwachsenerKind(Kind k, Erwachsener e, MainFrame frame) {
        try {
            Connection c = User.getConnectionFromUser();
            PreparedStatement pre = c.prepareStatement("Delete From erwachsener_has_kind where Kind_idKind=? AND Erwachsener_idErwachsener=?");
            pre.setInt(1, k.getIdKind());
            pre.setInt(2, e.getIdErwachsener());
            pre.executeUpdate();
            c.commit();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DeleteMethods.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
